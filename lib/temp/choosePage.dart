import 'package:flutter/material.dart';
import 'package:hivetemplate/views/quran_view.dart';

class ChooseVersionView extends StatefulWidget {
  const ChooseVersionView({Key? key}) : super(key: key);

  @override
  _ChooseVersionViewState createState() => _ChooseVersionViewState();
}

class _ChooseVersionViewState extends State<ChooseVersionView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: Column(
            children: [
              SizedBox(height: 75,),
              ElevatedButton(
                child: Text("النسخة الاولى"),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              QuranView(intialPage: 5, versionNumber: 1)));
                },
              ),
              SizedBox(
                height: 50,
              ),
              ElevatedButton(
                child: Text("النسخة الثانية "),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              QuranView(intialPage: 1, versionNumber: 2)));
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
