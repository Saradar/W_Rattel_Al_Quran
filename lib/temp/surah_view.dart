
import 'package:flutter/material.dart';
import 'package:hivetemplate/models/ayah/ayah.dart';
import 'package:provider/provider.dart';

class SurahAyats extends StatelessWidget {
  final List<Ayah>? ayatsList;
  final String? surahName;
  const SurahAyats(
      {Key? key,
        this.ayatsList,
        this.surahName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {


    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white,
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              leading: BackButton(
              ),
              backgroundColor:
              Colors.grey[850],
              pinned: true,
              expandedHeight: height * 0.27,
              flexibleSpace: flexibleAppBar(context, height, width),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                      (context, index) => line(height, index, width),
                  childCount: ayatsList!.length),
            )
          ],
        ));
  }

  Widget line(double height, int index, double width) {
    return Padding(
      padding: EdgeInsets.fromLTRB(width * 0.015, 0, 0, 0),
      child: Container(
        child: ListTile(
          trailing: CircleAvatar(
            radius: height * 0.013,
            backgroundColor: const Color(0xff04364f),
            child: CircleAvatar(
                radius: height * 0.012,
                backgroundColor: Colors.white,
                child: Text(
                  ayatsList![index].page.toString(),
                  style: TextStyle(fontSize: height * 0.015),
                )),
          ),
          title: Text(ayatsList![index].text!,
              textAlign: TextAlign.right,
              style: TextStyle(fontSize: height * 0.03, color: Colors.black)),
        ),
      ),
    );
  }

  Widget flexibleAppBar(BuildContext context, double width, double height) {
    return FlexibleSpaceBar(
        centerTitle: true,
        title: Text(surahName!,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: height * 0.045)),
        background: Stack(
          children: <Widget>[

            infoInAppBar(context),
          ],
        ));
  }

  Widget infoInAppBar(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(surahName!),
          Text(
            surahName!,
            style: Theme.of(context).textTheme.headline1,
          )
        ],
      ),
    );
  }


}
