
import 'package:flutter/material.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:hivetemplate/temp/surah_view.dart';
import 'package:provider/provider.dart';

import '../controller/quran_controller.dart';
import '../models/surah/surah.dart';
import '../models/surah/surah_list.dart';
import 'dart:developer' as developer;

class SurahIndex extends StatefulWidget {
  const SurahIndex({Key? key}) : super(key: key);

  @override
  State<SurahIndex> createState() => _SurahIndexState();
}

class _SurahIndexState extends State<SurahIndex> {
  final _hiveBox = Hive.box('data');
  List<Surah>? _surahs = [];

  @override
  void initState() {

    _getSurahData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.22,
                ),
                child: ListView.separated(
                  separatorBuilder: (context, index) {
                    return const Divider(
                      color: Color(0xffee8f8b),
                      height: 2.0,
                    );
                  },
                  itemCount: _surahs!.length,
                  itemBuilder: (context, index) {
                    return Container(
                      child: ListTile(
                        onLongPress: () => _surahInforBox(index),
                        leading: Text(
                          "${_surahs![index].name}",
                          style: TextStyle(color: Colors.red),

                        ),

                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SurahAyats(
                                ayatsList: _surahs![index].ayat,
                                surahName: _surahs![index].name,
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  },
                ),
              ),


            ],
          ),
        ));
  }

  void _surahInforBox(int index) {
    showDialog(
      context: context,
      builder: (context) => SurahInformation(
        surahNumber: _surahs![index].number,
        arabicName: "${_surahs![index].name}",
        ayat: _surahs![index].ayat!.length,

      ),
    );
  }

  // getting data
  Future<void> _getSurahData() async {
    SurahsList? _cacheSurahList = await _hiveBox.get('surahList');
    if (_cacheSurahList == null || _cacheSurahList.surahs!.isEmpty) {
      SurahsList _newSurahsList = await QuranAPI.getSurahList();
      if (mounted) {
        setState(() {
          _surahs = _newSurahsList.surahs;
        });
      }
    } else {
      if (mounted) {
        setState(() {
          _surahs = _cacheSurahList.surahs;
        });
      }
    }
  }

}

class SurahInformation extends StatefulWidget {
  final int? surahNumber;
  final String? arabicName;

  final int? ayat;


  const SurahInformation(
      {Key? key,
        this.arabicName,
        this.surahNumber,
        this.ayat,
       })
      : super(key: key);

  @override
  _SurahInformationState createState() => _SurahInformationState();
}

class _SurahInformationState extends State<SurahInformation>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return ScaleTransition(
      scale: scaleAnimation,
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: Container(
            padding:
            const EdgeInsets.symmetric(horizontal: 18.0, vertical: 10.0),
            width: width * 0.75,
            height: height * 0.37,
            decoration: ShapeDecoration(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  "Surah Information",
                  style: Theme.of(context).textTheme.headline2,
                ),
                SizedBox(
                  height: height * 0.02,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    Text(
                      widget.arabicName!,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ],
                ),
                Text("Ayahs: ${widget.ayat}"),
                Text("Surah Number: ${widget.surahNumber}"),

                SizedBox(
                  height: height * 0.02,
                ),
                SizedBox(
                  height: height * 0.05,
                  child: ElevatedButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text("OK")),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
