import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class MainTheme {
  static final ThemeData finalTheme =
      ThemeData(primaryColor: const Color.fromRGBO(153, 34, 88, 1),fontFamily: 'Cairo', accentColor:  const Color.fromRGBO(153, 34, 88, 1) );

  static Color primaryColor = const Color.fromRGBO(153, 34, 88, 1);
  static Color secondyColor = const Color.fromRGBO(255, 232, 181, 1);
  static final mainTextStyle =
      TextStyle(fontSize: 20, color: primaryColor, fontWeight: FontWeight.w800);
  static final mainDecoration = BoxDecoration(
    color: MainTheme.secondyColor,
    borderRadius: BorderRadius.all(Radius.circular(20)),
  );
  static String mainLogo = 'assets/svg/wa rattel logo 1.svg';
  static String splashScreen = 'assets/svg/SplashScreen.png';
  static String homeIcon2 = 'assets/svg/Home2.svg';
  static String homeIcon3 = 'assets/svg/Home3.svg';
  static String homeIcon4 = 'assets/svg/Home4.svg';
  static String homeQuran = 'assets/svg/HomeQuran.svg';
  static String homeDouaa = 'assets/svg/HomeDouaa.svg';
  static String homeAudio = 'assets/svg/HomeAudio.svg';
  static String dayDouaa = 'assets/svg/DayDouaa.svg';
  static String nightDouaa = 'assets/svg/NightDouaa.svg';
  static String ArabicDesign = 'assets/svg/ArabicDesign1.svg';
  static String ArabicDesign2 = 'assets/svg/ArabicDesign2.svg';
  static String Reading = 'assets/svg/Reading.svg';
  static String Audio = 'assets/svg/Audio2.svg';
  static String whiteArabicDesign = 'assets/svg/WhiteArabicDesign.svg';
  static String backgroundIcon = 'assets/svg/Rectangle 52.png';
  static String goldArabicDesign = 'assets/svg/goldenDesign.png';
  static String waqf = 'assets/svg/waqf.png';
}
