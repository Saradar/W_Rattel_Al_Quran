import 'dart:io';

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:hivetemplate/controller/storage_service.dart';
import 'package:hivetemplate/views/azkar_list_view.dart';
import 'package:path_provider/path_provider.dart';

import '../controller/prayers_times_controller.dart';
import '../models/audio/audio_model.dart';
import '../models/azkar/azkar_list_model.dart';
import '../models/azkar/azkar_model.dart';
import '../models/bookmark/bookmark_list_model.dart';
import '../models/bookmark/bookmark_model.dart';
import 'dart:developer' as developer;

import '../models/bookmark/new_bookmarks_list.dart';
import '../models/prayers_times/prayers_times_model.dart';
import '../models/prayers_times/prayers_timezone_model.dart';
import '../models/quran_versions/quran_versions_model.dart';

class ConstantData {
  static final List<String> surahsName = [
    'الفاتحة',
    'البقرة',
    'ال عمران',
    'النساء',
    'المَائدة',
    'الأنعَام',
    'الأعرَاف',
    'الأنفَال',
    'التوبَة',
    'يُونس',
    'هُود',
    'يُوسُف',
    'الرَّعْد',
    'إبراهِيم',
    'الحِجْر',
    'النَحْل',
    'الإسْرَاء',
    'الكهْف',
    'مَريَم',
    'طه',
    'الأنبيَاء',
    'الحَج',
    'المُؤمنون',
    'النُور',
    'الفُرْقان',
    'الشُّعَرَاء',
    'النَمْل',
    'القَصَص',
    'العَنكبوت',
    'الرُّوم',
    'لقمَان',
    'السَجدَة',
    'الأحزَاب',
    'سَبَأ',
    'فَاطِر',
    'يس',
    'الصَافات',
    'ص',
    'الزُمَر',
    'غَافِر',
    'فُصِلَتْ',
    'الشُورَى',
    'الزُّخْرُف',
    'الدخَان',
    'الجَاثيَة',
    'الأحْقاف',
    'محَمَد',
    'الفَتْح',
    'الحُجرَات',
    'ق',
    'الذاريَات',
    'الطُور',
    'النَجْم',
    'القَمَر',
    'الرَّحمن',
    'الوَاقِعَة',
    'الحَديد',
    'المجَادلة',
    'الحَشر',
    'المُمتَحنَة',
    'الصَّف',
    'الجُمُعَة',
    'المنَافِقون',
    'التغَابُن',
    'الطلَاق',
    'التحْريم',
    'المُلْك',
    'القَلَم',
    'الحَاقَة',
    'المعَارج',
    'نُوح',
    'الجِن',
    'المُزَّمِّل',
    'المُدثِر',
    'القِيَامَة',
    'الإنسَان',
    'المُرسَلات',
    'النبَأ',
    'النّازعَات',
    'عَبَس',
    'التَكوير',
    'الانفِطار',
    'المطفِّفِين',
    'الانْشِقَاق',
    'البرُوج',
    'الطَارِق',
    'الأَعْلى',
    'الغَاشِية',
    'الفَجْر',
    'البَلَد',
    'الشَمْس',
    'الليْل',
    'الضُحَى',
    'الشَرْح',
    'التِين',
    'العَلَق',
    'القَدْر',
    'البَينَة',
    'الزلزَلة',
    'العَادِيات',
    'القَارِعة',
    'التَّكَاثر',
    'العَصْر',
    'الهُمَزَة',
    'الفِيل',
    'قُرَيْش',
    'المَاعُون',
    'الكَوْثَر',
    'الكَافِرُون',
    'النَّصر',
    'المَسَد',
    'الإخْلَاص',
    'الفَلَق',
    'النَّاس',
  ];
  static final List<String> vocals = [
    'بدر التركي',
    'عبدالله خياط',
    'عبدالرحمن مسعد',
    'اسلام صبحي',
    'عبدالعزيز العسيري',
    'رعد الكردي',
  ];
  static final _hiveBoxAudioFiles = Hive.box('audioFilesHistory');
  static final _hiveBoxBookmark = Hive.box('history');
  static final _hiveBoxAzkar = Hive.box('azkarHistory');
  static final _hiveBoxPrayers = Hive.box('prayersTimesHistory');
  static final _hiveBoxQuranVersions = Hive.box('quranVersionsHistory');
  static final _hiveBoxPrayersTimezone = Hive.box('prayersTimezoneHistory');
  static int currentQuranVersion = 1;
  static bool isDefaultFilesDownloaded = false;
  static late final dir;
  static List audioFiles = List.generate(6, (_) => List.generate(114, (_) => false));
  static List<bool>quranVersionsList = [true,false,false];
  static List<String>prayersTimeZone = ['غير معرف','غير معرف'];
  static var audios_0 = <Audio>[];
  static var audios_1 = <Audio>[];
  static var audios_2 = <Audio>[];
  static var audios_3 = <Audio>[];
  static var audios_4 = <Audio>[];
  static var audios_5 = <Audio>[];
  static List<Bookmark> bookmarks = [];
  static List<String>audioLinks0 = [];
  static List newBookmarks = List.generate(625, (index) => false);
  static List audioFileCurrentIndex0 = List.generate(114, (index) => -1);
  static List audioFileCurrentIndex1 = List.generate(114, (index) => -1);
  static List audioFileCurrentIndex2 = List.generate(114, (index) => -1);
  static List audioFileCurrentIndex3 = List.generate(114, (index) => -1);
  static List audioFileCurrentIndex4 = List.generate(114, (index) => -1);
  static List audioFileCurrentIndex5 = List.generate(114, (index) => -1);

   static  AssetsAudioPlayer assetsAudioPlayer = AssetsAudioPlayer.newPlayer();

  static List existSurahList0 = [];
  static List existSurahList1 = [];
  static List existSurahList2 = [100,107,2,3,78,87,88];
  static List existSurahList3 = [13,17,18,26,31,32,41,42,44,54,55,56,59,64,66,67,68,70,72,76,79,85,88];
  static List existSurahList4 = [100,107,2,3,32,78,87,88];
  static List existSurahList5 = [];
  static List<AzkarModel> azkarList = [
    AzkarModel(name: 'سبحان الله', counter: 0),
    AzkarModel(name: 'الحمدلله', counter: 0),
    AzkarModel(name: 'لا إله إلا الله', counter: 0),
    AzkarModel(name: 'الله أكبر', counter: 0),
    AzkarModel(name: 'أستغفر الله', counter: 0),
    AzkarModel(name: 'الصلاة على النبي', counter: 0),
  ];
  static PrayersTimesModel currentPrayersTimes = PrayersTimesModel(
      fajar: "-:-",
      sunrise: "-:-",
      duhur: "-:-",
      asr: "-:-",
      sunset: "-:-",
      maghrib: "-:-",
      ishaa: "-:-");
  static Future initilizeDirectorey()async{
    dir = await getApplicationDocumentsDirectory();
  }
  static Future<void> initilizeAudioList()async{
    audios_0.clear();
    audios_1.clear();
    audios_2.clear();
    audios_3.clear();
    audios_4.clear();
    audios_5.clear();
    int cnt = 0;
    List<bool>a0 = audioFiles[0];
    List<bool>a1 = audioFiles[1];
    List<bool>a2 = audioFiles[2];
    List<bool>a3 = audioFiles[3];
    List<bool>a4 = audioFiles[4];
    List<bool>a5 = audioFiles[5];
    for(int i = 0 ; i<114;i++){
      if(a0[i] == true){
        developer.log('surah :${i} , index:${cnt}');
          audioFileCurrentIndex0[i] = cnt;
          cnt++;
          audios_0.add(Audio.file(
            '${dir.path}/0/$i.mp3',
            metas: Metas(
              id: i.toString(),
              title: surahsName[i],
              artist: vocals[0]
            )
          ));
      }
    }
    cnt = 0;
    for(int i = 0 ; i<114;i++){
      if(a1[i] == true){
        developer.log('surah :${i} , index:${cnt}');
        audioFileCurrentIndex1[i] = cnt;
        cnt++;
        audios_1.add(Audio.file(
            '${dir.path}/1/$i.mp3',
            metas: Metas(
                id: i.toString(),
                title: surahsName[i],
                artist: vocals[1]
            )
        ));
      }
    }
    cnt = 0;
    for(int i = 0 ; i<114;i++){
      if(a2[i] == true){
        developer.log('surah :${i} , index:${cnt}');
        audioFileCurrentIndex2[i] = cnt;
        cnt++;
        audios_2.add(Audio.file(
            '${dir.path}/2/$i.mp3',
            metas: Metas(
                id: i.toString(),
                title: surahsName[i],
                artist: vocals[2]
            )
        ));
      }
    }
    cnt = 0;
    for(int i = 0 ; i<114;i++){
      if(a3[i] == true){
        developer.log('surah :${i} , index:${cnt}');
        audioFileCurrentIndex3[i] = cnt;
        cnt++;
        audios_3.add(Audio.file(
            '${dir.path}/3/$i.mp3',
            metas: Metas(
                id: i.toString(),
                title: surahsName[i],
                artist: vocals[3]
            )
        ));
      }
    }
    cnt = 0;
    for(int i = 0 ; i<114;i++){
      if(a4[i] == true){
        developer.log('surah :${i} , index:${cnt}');
        audioFileCurrentIndex4[i] = cnt;
        cnt++;
        audios_4.add(Audio.file(
            '${dir.path}/4/$i.mp3',
            metas: Metas(
                id: i.toString(),
                title: surahsName[i],
                artist: vocals[4]
            )
        ));
      }
    }
    cnt = 0;
    for(int i = 0 ; i<114;i++){
      if(a5[i] == true){
        developer.log('surah :${i} , index:${cnt}');
        audioFileCurrentIndex5[i] = cnt;
        cnt++;
        audios_5.add(Audio.file(
            '${dir.path}/5/$i.mp3',
            metas: Metas(
                id: i.toString(),
                title: surahsName[i],
                artist: vocals[5]
            )
        ));
      }
    }

  }


  static Future<void> initilizePrayersTimeZone()async{
    PrayersTimezone? temp = await _hiveBoxPrayersTimezone.get('prayersTimezone');
    if(temp !=null){
      prayersTimeZone[0] = temp.country;
      prayersTimeZone[1] = temp.capital;
    }
  }
  static void initilizeExistSurah(){
    List notExist5 = [4,5,6,7,8,9,10,11,14,15,16,24,37,40,42,56];
    for(int i=1;i<=114;i++){
      if(!notExist5.contains(i)){
        existSurahList5.add(i);
      }
    }
    for(int i =1;i<=114;i++){
      existSurahList0.add(i);
      existSurahList1.add(i);
    }
  }
  static Future saveLocal(localFileName) async {
    final dir = await getApplicationDocumentsDirectory();
    final file = new File("${dir.path}/$localFileName");
    if (!(await file.exists())) {
      final soundData = await rootBundle.load("assets/$localFileName");
      final bytes = soundData.buffer.asUint8List();
      await file.writeAsBytes(bytes, flush: true);
    }
  }
  static Future<void> updatePrayersTimezone()async{
    await _hiveBoxPrayersTimezone.clear();
    PrayersTimezone temp = PrayersTimezone(country: prayersTimeZone[0],capital: prayersTimeZone[1]);
    await _hiveBoxPrayersTimezone.put('prayersTimezone', temp);
  }
  static Future<void> initilizeQuranVersions()async{
    QuranVersions? temp = await _hiveBoxQuranVersions.get('quranVersions');
    if(temp!=null){
      quranVersionsList = temp.quranVersionsList;
      currentQuranVersion = temp.currentVersionIndex+1;
    }
  }

  static Future<void> updateQuranVersions()async{
    await _hiveBoxQuranVersions.clear();
    QuranVersions temp = QuranVersions(quranVersionsList: quranVersionsList,currentVersionIndex: currentQuranVersion-1);
    await _hiveBoxQuranVersions.put('quranVersions', temp);
  }

  static Future<void> intilizeBookmarks() async {
    BookmarkListModel? temp = await _hiveBoxBookmark.get('bookmarks');
    BookmarkList? temp1 = await _hiveBoxBookmark.get('bookmarksView');
    if (temp != null) {
      newBookmarks = temp.bookmarks;
    }
    if(temp1!=null){
      bookmarks = temp1.bookmarkList!;
    }

  }

  static Future<void> initilizeDownloadedAudioFiles() async {
    AudioModel?temp = await _hiveBoxAudioFiles.get('audioFiles');
    if(temp !=null){
      audioFiles = temp.audioFiles;
    }

  }
  static Future<void>updateDownloadAudioFiles()async{
    await _hiveBoxAudioFiles.clear();
    AudioModel temp = AudioModel(audioFiles: audioFiles);
    await _hiveBoxAudioFiles.put('audioFiles', temp);

  }

  static Future<void> intilizePrayersTimesData() async {
    // PrayersTimesModel model = await _hiveBoxPrayers.get('prayersTimes');
    // if (model == null) {
    //   PrayersTimesModel onlineModel =
    //       await PrayersTimesController.getPrayersTimes();
    //   currentPrayersTimes = onlineModel;
    // } else {
    //   currentPrayersTimes = model;
    // }
  }

  static Future<void> updatePrayersTime(PrayersTimesModel onlineModel) async {
    _hiveBoxPrayers.clear();
    currentPrayersTimes = onlineModel;
    await _hiveBoxPrayers.put('prayersTimes', onlineModel);
  }

  static Future<void> updateBookmarks() async {
    await _hiveBoxBookmark.clear();
    BookmarkListModel temp = BookmarkListModel(bookmarks: newBookmarks);
    await _hiveBoxBookmark.put('bookmarks', temp);
    BookmarkList temp1 = BookmarkList(bookmarkList: bookmarks);
    await _hiveBoxBookmark.put('bookmarksView', temp1);

  }

  static String getTheCurrentQuranVersion() {
    if (currentQuranVersion == 1)
      return 'أخضر';
    else if (currentQuranVersion == 2)
      return 'أزرق';
    else
      return 'أسود';
  }

  static Future<List<Bookmark>> clearHistory() async {
    bookmarks.clear();
    for(int i =0 ; i<625;i++){
      newBookmarks[i] = false;
    }
    await updateBookmarks();
    return bookmarks;
  }

  static Future<void> intilizeAzkar() async {

    AzkarListModel? temp = await _hiveBoxAzkar.get('azkar');
    if (temp != null) {
      azkarList = temp.azkarList!;
    }
  }

  static Future<void> updateAzkar() async {
    await _hiveBoxAzkar.clear();
    AzkarListModel temp = AzkarListModel(azkarList: azkarList);
    await _hiveBoxAzkar.put('azkar', temp);
  }

  static Future<void> saveAzkar(String name, int counter) async {
    if (name == 'سبحان الله')
      azkarList[0].updateCounter(counter);
    else if (name == 'الحمدلله')
      azkarList[1].updateCounter(counter);
    else if (name == 'لا إله إلا الله')
      azkarList[2].updateCounter(counter);
    else if (name == 'الله أكبر')
      azkarList[3].updateCounter(counter);
    else if (name == 'أستغفر الله')
      azkarList[4].updateCounter(counter);
    else if (name == 'الصلاة على النبي') azkarList[5].updateCounter(counter);
  }
  static Future<void> initilizeDefaultDownload()async{
    final file1 = new File("${ConstantData.dir.path}/quranVersions/NewGreen.pdf");
    final file2 = new File("${ConstantData.dir.path}/rukia/rukia.mp3");

    if(!(await file1.exists())||!(await file2.exists()))
      ConstantData.isDefaultFilesDownloaded = false;
    else
      ConstantData.isDefaultFilesDownloaded = true;

  }

  static Future<void> initilizePrayersTimes()async{
    PrayersTimesModel? temp  = await _hiveBoxPrayers.get('prayersTimes');
    if(temp!=null)
      currentPrayersTimes = temp;
  }



}
