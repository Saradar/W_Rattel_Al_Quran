
import 'dart:ui';

import 'package:adhan/adhan.dart';
import 'package:hive/hive.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'dart:developer'as developer;

import '../models/prayers_times/prayers_times_model.dart';

class AladhanController{
  static Future<void>getCurrentTimes(double lat ,double lon)async{


       final myCoordinates = Coordinates(lat, lon);
       final params = CalculationMethod.karachi.getParameters();
       params.madhab = Madhab.hanafi;
       final prayerTimes = PrayerTimes.today(myCoordinates, params);


       print("---Today's Prayer Times in Your Local Timezone(${prayerTimes.fajr.timeZoneName})---");
       print(DateFormat.jm().format(prayerTimes.fajr));
       print(DateFormat.jm().format(prayerTimes.sunrise));
       print(DateFormat.jm().format(prayerTimes.dhuhr));
       print(DateFormat.jm().format(prayerTimes.asr));
       print(DateFormat.jm().format(prayerTimes.maghrib));
       print(DateFormat.jm().format(prayerTimes.isha));
       PrayersTimesModel current = PrayersTimesModel(
           fajar:DateFormat.jm().format(prayerTimes.fajr),
           sunrise:DateFormat.jm().format(prayerTimes.sunrise),
           duhur: DateFormat.jm().format(prayerTimes.dhuhr),
           asr: DateFormat.jm().format(prayerTimes.asr),
         maghrib: DateFormat.jm().format(prayerTimes.maghrib),
         ishaa: DateFormat.jm().format(prayerTimes.isha),
         sunset: DateFormat.jm().format(prayerTimes.sunrise)
       );
       ConstantData.currentPrayersTimes = current;
       await ConstantData.updatePrayersTime(current);

     }
  }
