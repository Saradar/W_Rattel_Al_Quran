import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:hivetemplate/models/prayers_times/prayers_times_model.dart';
import 'package:hivetemplate/utils/loader_dialog.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;

class PrayersTimesController {
  static final _hiveBox = Hive.box('prayersTimesHistory');

  static Future<PrayersTimesModel> getPrayersTimes(double lat ,double lon ) async {
    var currentDate = DateTime.now();
    var formatter = DateFormat('dd-MM-yyyy');

    //var formatter = DateFormat('yyyy-MM-dd');
    String formattedDate = formatter.format(currentDate);
    developer.log(formattedDate);
    String counrty = 'Syria';
    String capital = 'Damascus';
    if(ConstantData.prayersTimeZone[0] == 'مصر'){
      counrty = 'Egypt';
      capital = 'Cairo';
    }

    else if(ConstantData.prayersTimeZone[0] == 'السعودية'){
      counrty = 'KSA';
      capital = 'Mekka';
    }
    else if(ConstantData.prayersTimeZone[0] == 'الامارات'){
      counrty = 'UAE';
      capital = 'Dubai';
    }
    String newUrl  = 'https://api.aladhan.com/v1/timings/date_or_timestamp=${formattedDate}?latitude=${lat}&longitude=${lon}&method=4&school=0';
    String url =
        'https://api.aladhan.com/v1/timingsByAddress/$formattedDate?address=${capital},${counrty}&method=8';


    http.Response response = await http.get(Uri.parse(newUrl));

    if(response.statusCode == 200){
      developer.log("here21");
      await _hiveBox.put('prayersTimes', PrayersTimesModel.fromJSON(jsonDecode(response.body)));
      ConstantData.currentPrayersTimes = PrayersTimesModel.fromJSON(jsonDecode(response.body));
      developer.log("Prayers Times Done");
      return PrayersTimesModel.fromJSON(jsonDecode(response.body));
    }
    else{
      throw Exception("Failed  to Load Prayers Times ");
    }
  }

  static Future<void> getTimesData()async{
  //   PrayersTimesModel? temp  = await _hiveBox.get('prayersTimes');
  //   if(temp == null){
  //     await getPrayersTimes();
  //   }
  //   else {
  //     ConstantData.currentPrayersTimes = temp;
  //   }
  }
  static String FixTheTime(String time){
    int hours = int.parse(time[0]+time[1]);
    int mins = int.parse(time[3]+time[4]);

    mins+=20;

    if(mins>59){
      hours++;
      mins-=60;
    }

    if(hours>12)
      hours -= 12;
    String stringHours ;
    String stringMins ;

    if(hours<10){
      stringHours ='0'+ hours.toString();
    }
    else
      stringHours = hours.toString();

    if(mins<10){
      stringMins = '0'+ mins.toString();
    }
    else
      stringMins = mins.toString();


    String fixedTime = "$stringHours:$stringMins";

    return fixedTime;

  }
}

