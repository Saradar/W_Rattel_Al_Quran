import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:hivetemplate/constants/MainTheme.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'dart:developer'as developer;
class ConnectionController{
  static Future<bool>hasInternet()async{
    var result =  await Connectivity().checkConnectivity();
    developer.log('message');
    if(result == ConnectivityResult.wifi || result == ConnectivityResult.mobile)
      return true;
    else
      return  false;

  }
  static Future<bool>hasInternetConnection()async{
    return await InternetConnectionChecker().hasConnection;

  }
  static  showConnectionAlert(BuildContext context){
    developer.log('121e3');
      Widget okButton =  TextButton(
        child: Text('حسنا',style: MainTheme.mainTextStyle,),
        onPressed: (){
          Navigator.pop(context);
        },
      );
      AlertDialog alert = AlertDialog(
        title: Icon(Icons.wifi_off,color:MainTheme.primaryColor,size: 35,),
        content: Text("لا يوجد اتصال بالانترنت",textAlign:TextAlign.center,style: TextStyle(fontWeight: FontWeight.w700,fontSize: 20,color: MainTheme.primaryColor)),
        actions: [
          okButton,
        ],
      );
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
  }
}