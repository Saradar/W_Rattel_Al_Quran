import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'dart:developer' as developer;

import 'package:hivetemplate/utils/loader_dialog.dart';

class CloudStorage {
  static final firebase_storage.FirebaseStorage storage =
      firebase_storage.FirebaseStorage.instance;

  static Future<String> downloadAudioFileUrl(
      int vocalIndex, int surahIndex) async {
    if(vocalIndex==5)
      vocalIndex = 6;
    String url = await storage
        .ref('${vocalIndex}/${surahIndex + 1}.mp3')
        .getDownloadURL();
    developer.log(url);
    return url;


  }

  static Future<String> downloadRukiaAudioUrl() async {
    String url = await storage
        .ref('rukia/RuqiaAudio.mp3')
        .getDownloadURL();
    developer.log(url);
    return url;
  }

  static Future<String> downloadDefaultQuranUrl()async{
    String url = await storage
        .ref('quran/NewGreen2.pdf')
        .getDownloadURL();
    developer.log(url);
    return url;
  }
  static Future<String> downloadBlackQuranUrl()async{
    String url = await storage
        .ref('quran/Black.pdf')
        .getDownloadURL();
    developer.log(url);
    return url;
  }
  static Future<String> downloadBlueQuranUrl()async{
    String url = await storage
        .ref('quran/NewBlue.pdf')
        .getDownloadURL();
    developer.log(url);
    return url;
  }

}
