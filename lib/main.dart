import 'dart:async';
import 'dart:io';
import 'package:adhan/adhan.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:dio/dio.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:hivetemplate/constants/MainTheme.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:hivetemplate/controller/prayers_times_controller.dart';
import 'package:hivetemplate/controller/quran_controller.dart';
import 'package:hivetemplate/models/audio/audio_model.dart';
import 'package:hivetemplate/models/ayah/ayah.dart';
import 'package:hivetemplate/models/prayers_times/prayers_times_model.dart';
import 'package:hivetemplate/temp/choosePage.dart';
import 'package:hivetemplate/temp/temp.dart';
import 'package:hivetemplate/utils/loader_dialog.dart';
import 'package:hivetemplate/views/Prayers_times_view.dart';
import 'package:hivetemplate/views/audio/audioPlayer.dart';
import 'package:hivetemplate/views/audio_files_option_view.dart';
import 'package:hivetemplate/views/azkar_list_view.dart';
import 'package:hivetemplate/views/azkar_list_view2.dart';
import 'package:hivetemplate/views/azkar_view.dart';
import 'package:hivetemplate/views/bookmark_view.dart';
import 'package:hivetemplate/views/home_view.dart';
import 'package:hivetemplate/views/juz_list_view.dart';
import 'package:hivetemplate/views/quran_options_view.dart';
import 'package:hivetemplate/views/quran_view.dart';
import 'package:hivetemplate/views/rukia_view.dart';
import 'package:hivetemplate/views/search_view.dart';
import 'package:hivetemplate/temp/surah_list_view.dart';
import 'package:hivetemplate/views/settings_view.dart';
import 'package:hivetemplate/views/settings_view2.dart';
import 'package:hivetemplate/views/subhaHistory.dart';
import 'package:hivetemplate/views/subhaView2.dart';
import 'package:hivetemplate/views/subha_view.dart';
import 'package:hivetemplate/views/surah_list_view2.dart';
import 'package:splash_screen_view/SplashScreenView.dart';
import 'controller/connection_controller.dart';
import 'controller/storage_service.dart';
import 'models/azkar/azkar_list_model.dart';
import 'models/azkar/azkar_model.dart';
import 'models/bookmark/bookmark_list_model.dart';
import 'models/bookmark/bookmark_model.dart';
import 'models/bookmark/new_bookmarks_list.dart';
import 'models/page/page_list.dart';
import 'models/prayers_times/prayers_timezone_model.dart';
import 'models/quran_versions/quran_versions_model.dart';
import 'models/surah/surah.dart';
import 'models/surah/surah_list.dart';
import 'dart:developer' as developer;
import 'package:flutter/services.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  AssetsAudioPlayer.setupNotificationsOpenAction((notification) {
    return true;
  });
  await Firebase.initializeApp();


  //intilize the local DataBase and The Models
  await Hive.initFlutter();
  Hive.registerAdapter<Ayah>(AyahAdapter());
  Hive.registerAdapter<SurahsList>(SurahsListAdapter());
  Hive.registerAdapter<Surah>(SurahAdapter());
  Hive.registerAdapter<PrayersTimesModel>(PrayersTimesModelAdapter());
  Hive.registerAdapter<Bookmark>(BookmarkAdapter());
  Hive.registerAdapter<BookmarkList>(BookmarkListAdapter());
  Hive.registerAdapter<AzkarModel>(AzkarModelAdapter());
  Hive.registerAdapter<AzkarListModel>(AzkarListModelAdapter());
  Hive.registerAdapter<AudioModel>(AudioModelAdapter());
  Hive.registerAdapter<QuranVersions>(QuranVersionsAdapter());
  Hive.registerAdapter<PrayersTimezone>(PrayersTimezoneAdapter());
  Hive.registerAdapter<BookmarkListModel>(BookmarkListModelAdapter());

  //Accsess the local DataBase
  await Hive.openBox('data');
  await Hive.openBox('history');
  await Hive.openBox('azkarHistory');
  await Hive.openBox('prayersTimesHistory');
  await Hive.openBox('audioFilesHistory');
  await Hive.openBox('quranVersionsHistory');
  await Hive.openBox('prayersTimezoneHistory');

  await ConstantData.initilizeDirectorey();
  await ConstantData.initilizeDefaultDownload();

  await QuranAPI.getQuranData();
  await QuranAPI.getAllAyahs();
  //Get QuranVersions Data from DB
  await ConstantData.initilizeQuranVersions();
  //Get All Bookmarks from DB
  await ConstantData.intilizeBookmarks();

  //Get All Azkar from DB
  await ConstantData.intilizeAzkar();
  //Get All Downloaded Audio Files
  await ConstantData.initilizeDownloadedAudioFiles();
  await ConstantData.initilizeAudioList();
  //await ConstantData.initilizeAudioList();
  await ConstantData.initilizePrayersTimes();
  //Get the current Prayers Times
  //await PrayersTimesController.getTimesData();
  //Is Default Files Downloaded
  await ConstantData.initilizeDefaultDownload();

  ConstantData.initilizeExistSurah();

  runApp(const MyApp());
}

class IntroScreen extends StatefulWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  bool showDownloadBar = false;
  double percentage = 0.0;
  String downloadingPercentage = '0';
  @override
  void initState() {
    // TODO: implement initStated

    if(!ConstantData.isDefaultFilesDownloaded)
    {
      downloadDefaultFiles();

    }
     else
       _navigateToHome();
    super.initState();
  }


  Future<void> downloadDefaultFiles() async {
    bool result =
    await ConnectionController
        .hasInternetConnection();
    if(!result)
      LoaderDialog.showExitErrorDialog(context);
    else{
    Dio dio = Dio();
    try {
      String url1 = await CloudStorage.downloadDefaultQuranUrl();
      var response = await dio.download(url1,"${ConstantData.dir.path}/quranVersions/NewGreen.pdf",deleteOnError: true,onReceiveProgress:(actualbytes,totalbytes){
        var per = actualbytes/totalbytes*100;
        setState(() {
          percentage = per/100;
          downloadingPercentage = (percentage*100).toInt().toString();
          showDownloadBar = true;
        });

      });
      if(response.statusCode==200) {
        try{
        String url2 = await CloudStorage.downloadRukiaAudioUrl();
        var response2 = await dio.download(
            url2, "${ConstantData.dir.path}/rukia/rukia.mp3", deleteOnError: true,
            onReceiveProgress: (actualbytes, totalbytes) {
              var per = actualbytes / totalbytes * 100;
              setState(() {
                percentage = per / 100;
                downloadingPercentage = (percentage*100).toInt().toString();
                developer.log(downloadingPercentage);
                showDownloadBar = true;
              });
            });
        if (response2.statusCode == 200) {
          _navigateToHome();
        } else
          LoaderDialog.showExitErrorDialog(context);}
          catch(e){
            LoaderDialog.showExitErrorDialog(context);
          }
      }
      else
        LoaderDialog.showExitErrorDialog(context);


    } catch (e) {
      LoaderDialog.showExitErrorDialog(context);
    }}
  }


  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Stack(
        children: [
          Container(
              height: height,
              width: width,
              color: MainTheme.primaryColor,
              child: Center(
                child: Column(
                  children: [
                    SizedBox(height: height*0.20,),
                    Center(child: SvgPicture.asset(MainTheme.mainLogo))
                  ],
                ),
              )),
          Column(
            children: [
              SizedBox(height: height*0.60,),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Center(
                  child: Center(child: Image.asset('assets/svg/waqf.png',scale: 0.5,)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Center(
                  child: Visibility(
                    visible: showDownloadBar,
                    child: LinearProgressIndicator(
                      color: MainTheme.secondyColor,
                      value: percentage,
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: showDownloadBar,
                child: Center(
                  child: Text('$downloadingPercentage%',style: TextStyle(color: MainTheme.secondyColor,fontSize: 20),textAlign: TextAlign.center,),),
              ),
              Visibility(
                visible: showDownloadBar,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('..يتم الان تنزيل ملفات القران والرقية الشرعية  ',style: TextStyle(color: MainTheme.secondyColor,fontSize: 18),textAlign: TextAlign.center,),

                      ],
                    )
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  _navigateToHome() async {
    await Future.delayed(Duration(seconds: 2), () {});
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/start', (Route<dynamic> route) => false);
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'W_Rattel AL_Quran',
      theme: MainTheme.finalTheme,
      initialRoute: '/splashScreen',
      routes: {
        '/splashScreen': (context) => IntroScreen(),
        '/start': (context) => const MyHomePage(),
        '/azkarView': (context) => const AzkarView(),
        '/rukiaView': (context) => const RukiaView(),
        '/prayersTimes': (context) => const PrayersTimesView(),
        '/subhaView': (context) => const SubhaView(),
        '/surahListView': (context) => const SurahListView(),
        '/azkarSabahListView': (context) => const AzkarListView(),
        '/azkarMassaaListView': (context) => const AzkarListView2(),
        '/quranOptionsView': (context) => const QuranOptionsView(),
        '/subhaHistoryView': (context) => const SubhaHistory(),
        '/juzListView': (context) => const JuzListView(),
        '/audioOptionView': (context) => const AudioOptionView(),
        '/temp': (context) => const SurahIndex(),
        '/template': (context) => const Template(),
        '/subhaTemp': (context) => const SubhaView2(),
      },
      //home:  MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int index = 1;
  final screens = [SettingsView2(), HomeView(), BookmarkHistoryView()];
  @override
  /*void initState() {
    // TODO: implement initState
    //getPagesData();
    super.initState();
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: NavigationBarTheme(
        data: NavigationBarThemeData(indicatorColor: MainTheme.primaryColor),
        child: NavigationBar(
          selectedIndex: index,
          onDestinationSelected: (index) => setState(() {
            this.index = index;
          }),
          destinations: [
            NavigationDestination(
              icon: Icon(Icons.settings),
              label: '',
            ),
            NavigationDestination(icon: Icon(Icons.home), label: ''),
            NavigationDestination(icon: Icon(Icons.history), label: '')
          ],
        ),
      ),
      body: screens[index],
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
