import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hivetemplate/main.dart';
import 'dart:developer' as developer;

import '../constants/MainTheme.dart';

class LoaderDialog {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    var wid = MediaQuery.of(context).size.width / 2;
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(left: 130, right: 130),
          child: Dialog(
              key: key,
              backgroundColor: Colors.white,
              child: Container(
                  width: 100,
                  height: 100,
                  child: Center(child: CircularProgressIndicator(color: MainTheme.primaryColor,)))),
        );
      },
    );
  }

  static showLocationUpdatedDialog(BuildContext context) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: Container(
              height: 200,
              width: 300,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(15)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.location_on_outlined,
                      size: 30,
                      color: MainTheme.primaryColor,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Center(
                        child: Text(
                      'تم تحديث الموقع لمواقيت الصلاة',
                      style: MainTheme.mainTextStyle,
                    )),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text(
                              'تم',
                              style: MainTheme.mainTextStyle,
                            ))
                      ],
                    ),
                  )
                ],
              )),
        );
      },
    );
  }

  static showTimeoutDialog(BuildContext context) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: Card(
            child: Container(
                height: 200,
                width: 300,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(25)),

                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.error,
                        size: 30,
                        color: MainTheme.primaryColor,
                      ),
                    ),


                         Padding(
                           padding: const EdgeInsets.all(8.0),
                           child: Text(
                      'حدث خطأ أثناء الاتصال .. يرجى المحاولة لاحقا',
                      style: MainTheme.mainTextStyle,
                             textAlign:TextAlign.center,
                    ),
                         ),
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                'تم',
                                style: MainTheme.mainTextStyle,
                              ))
                        ],
                      ),
                    )
                  ],
                )),
          ),
        );
      },
    );
  }
  static showNotAvailableNowDialog(BuildContext context) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: Card(
            child: Container(
                height: 200,
                width: 300,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(25)),

                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.error,
                        size: 30,
                        color: MainTheme.primaryColor,
                      ),
                    ),


                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'غير متوفر حاليا',
                        style: MainTheme.mainTextStyle,
                        textAlign:TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                'تم',
                                style: MainTheme.mainTextStyle,
                              ))
                        ],
                      ),
                    )
                  ],
                )),
          ),
        );
      },
    );
  }

  static showDownloadWaitingDialog(BuildContext context) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: Card(
            child: Container(
                height: 200,
                width: 300,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(25)),

                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.error,
                        size: 30,
                        color: MainTheme.primaryColor,
                      ),
                    ),


                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'الرجاء الانتظار حتى انتهاء التحميل',
                        style: MainTheme.mainTextStyle,
                        textAlign:TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                'تم',
                                style: MainTheme.mainTextStyle,
                              ))
                        ],
                      ),
                    )
                  ],
                )),
          ),
        );
      },
    );
  }

  static showDoneDialog(BuildContext context) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: Card(
            child: Container(
                height: 200,
                width: 300,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.done,
                        size: 30,
                        color: MainTheme.primaryColor,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Center(
                          child: Text(
                        'تم التنزيل',
                        style: MainTheme.mainTextStyle,
                            textAlign:TextAlign.center,
                      )),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                'تم',
                                style: MainTheme.mainTextStyle,
                              ))
                        ],
                      ),
                    )
                  ],
                )),
          ),
        );
      },
    );
  }

  static showUpdatedDoneDialog(BuildContext context) {
    // set up the button
    Widget cancelButton = TextButton(
      child: Text(
        "تم",
        style: MainTheme.mainTextStyle,
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Icon(
        Icons.done,
        color: MainTheme.primaryColor,
        size: 30,
      ),
      content: Text("تم التحديث ",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20,
              color: MainTheme.primaryColor)),
      actions: [cancelButton],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static showExitErrorDialog(BuildContext context) {
    // set up the button
    Widget cancelButton = TextButton(
      child: Text(
        "تم",
        style: MainTheme.mainTextStyle,
      ),
      onPressed: () {
        SystemNavigator.pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Icon(
        Icons.done,
        color: MainTheme.primaryColor,
        size: 30,
      ),
      content: Text(
          "حدث مشكلة أثناء التنزيل .. الرجاء التحقق من الانترنت والمحاولة لاحقا ",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20,
              color: MainTheme.primaryColor)),
      actions: [cancelButton],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
