import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hivetemplate/constants/MainTheme.dart';

class NextPrayerTime extends StatefulWidget {
  const NextPrayerTime({Key? key}) : super(key: key);

  @override
  _NextPrayerTimeState createState() => _NextPrayerTimeState();
}

class _NextPrayerTimeState extends State<NextPrayerTime> {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Container(
      height: height * 0.20,
      width: width * 0.88,
      decoration: BoxDecoration(
        color: MainTheme.secondyColor,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'الوقت المتبقي ب صلاة',
              textAlign: TextAlign.right,
              style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                  color: MainTheme.primaryColor),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      '2',
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontWeight: FontWeight.w800, fontSize: 35,color: MainTheme.primaryColor),
                    ),
                    Text(
                      'ساعة',
                      textAlign: TextAlign.center,
                      style:
                      TextStyle(fontWeight: FontWeight.w700, fontSize: 17,color: MainTheme.primaryColor),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      '2',
                      textAlign: TextAlign.center,
                      style:
                      TextStyle(fontWeight: FontWeight.w800, fontSize: 35,color: MainTheme.primaryColor),
                    ),
                    Text(
                      'ساعة',
                      textAlign: TextAlign.center,
                      style:
                      TextStyle(fontWeight: FontWeight.w700, fontSize: 17,color: MainTheme.primaryColor),
                    )
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
