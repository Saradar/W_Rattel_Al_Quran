import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hivetemplate/constants/MainTheme.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:location/location.dart';

import '../controller/aladhan_controller.dart';
import '../controller/connection_controller.dart';
import '../controller/prayers_times_controller.dart';
import '../models/prayers_times/prayers_times_model.dart';
import '../utils/loader_dialog.dart';

class PrayersTimes extends StatefulWidget {
  const PrayersTimes({Key? key}) : super(key: key);

  @override
  _PrayersTimesState createState() => _PrayersTimesState();
}

class _PrayersTimesState extends State<PrayersTimes> {

   PrayersTimesModel? _currentTimes = PrayersTimesModel(fajar: "-:-",sunrise: "-:-",duhur: "-:-",asr: "-:-",sunset: "-:-",maghrib: "-:-",ishaa: "-:-");
  @override
  void initState() {
    // TODO: implement initState

    setState(() {
      _currentTimes = ConstantData.currentPrayersTimes;
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(
          height: height*0.55,
          width: width*0.88,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(20)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(_currentTimes!.fajar ?? "-:-",
                      textAlign: TextAlign.right,style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 25,
                          color: MainTheme.primaryColor

                        ),),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'الفجر',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontSize: 19,
                          fontWeight: FontWeight.w700,
                            color: MainTheme.primaryColor
                        ),),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(_currentTimes!.duhur ?? "-:-",
                        textAlign: TextAlign.right,style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 25,
                            color: MainTheme.primaryColor

                        ),),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'الظهر',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontSize: 19,
                          fontWeight: FontWeight.w700,
                            color: MainTheme.primaryColor
                        ),),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(_currentTimes!.asr ?? "-:-",
                        textAlign: TextAlign.right,style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 25,
                            color: MainTheme.primaryColor

                        ),),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'العصر',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontSize: 19,
                          fontWeight: FontWeight.w700,
                            color: MainTheme.primaryColor
                        ),),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(_currentTimes!.maghrib ?? "-:-",
                        textAlign: TextAlign.right,style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 25,
                            color: MainTheme.primaryColor

                        ),),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'المغرب',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontSize: 19,
                          fontWeight: FontWeight.w700,
                            color: MainTheme.primaryColor
                        ),),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(_currentTimes!.ishaa ?? "-:-",
                        textAlign: TextAlign.right,style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: 25,
                            color: MainTheme.primaryColor

                        ),),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'العشاء',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontSize: 19,
                          fontWeight: FontWeight.w700,
                            color: MainTheme.primaryColor
                        ),),
                    )
                  ],
                ),


              ],
            ),
          ),
        ),

        Center(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: ElevatedButton(
              child: Icon(Icons.refresh,color: MainTheme.primaryColor,),
              style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  fixedSize: const Size(56,56),
                  shape:  const CircleBorder()
              ),
              onPressed: ()async{
                if(await ConnectionController.hasInternetConnection()){
                  Location location = Location();
                  bool _serviceEnabled;
                  PermissionStatus _permissionGranted;
                  LocationData _locationData;
                  _serviceEnabled = await location.serviceEnabled();
                  if (!_serviceEnabled) {
                    _serviceEnabled = await location.requestService();
                    if (!_serviceEnabled)
                      return;
                  }
                  _permissionGranted = await location.hasPermission();
                  if (_permissionGranted == PermissionStatus.denied) {
                    _permissionGranted = await location.requestPermission();
                    if (_permissionGranted != PermissionStatus.granted) {
                      return;
                    }
                  }
                  final GlobalKey<NavigatorState> _loaderDialog =
                  new GlobalKey<NavigatorState>();
                  _locationData = await location.getLocation();
                  LoaderDialog.showLoadingDialog(context, _loaderDialog);
                  //await AladhanController.getCurrentTimes(_locationData.latitude!, _locationData.longitude!);
                  try{
                  await PrayersTimesController.getPrayersTimes(_locationData.latitude!, _locationData.latitude!);
                  Navigator.pop(context);
                  LoaderDialog.showLocationUpdatedDialog(context);
                  setState(() {
                    _currentTimes = ConstantData.currentPrayersTimes;
                  });}
                  catch(e){
                    Navigator.pop(context);
                    LoaderDialog.showTimeoutDialog(context);
                  }
                }
                else
                  ConnectionController.showConnectionAlert(context);

              },
            ),
          ),
        )
      ],
    );
  }


}
