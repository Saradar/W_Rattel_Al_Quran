import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hivetemplate/constants/MainTheme.dart';

class HomeViewCard extends StatelessWidget {
  final String svgPath;
  final String text;
  final Function onTap;
  const HomeViewCard(
      {Key? key,
      required this.svgPath,
      required this.text,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return GestureDetector(
      child: Center(
        child: Container(
          height: 100,
          width: 0.88 * width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Center(
            child: Row(
              children: [
                SizedBox(
                  width: 15,
                ),
                Container(
                  child: SvgPicture.asset(svgPath),
                ),
                SizedBox(
                  width: 150,
                ),
                Text(text,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                        color: MainTheme.primaryColor))
              ],
            ),
          ),
        ),
      ),
      onTap: onTap(),
    );
  }
}
