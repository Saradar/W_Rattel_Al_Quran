import 'package:flutter/material.dart';
class ToolBar extends StatelessWidget {
  const ToolBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.black,
      toolbarHeight: 25,

      actions: [
        IconButton(
          icon:Icon(Icons.bookmark) ,
          iconSize: 25,
          onPressed: (){

          },
        ),
        IconButton(
          icon:Icon(Icons.remove_red_eye) ,
          iconSize: 25,
          onPressed: (){

          },
        ),
        IconButton(
          icon:Icon(Icons.search) ,
          iconSize: 25,
          onPressed: (){

          },
        ),
      ],

    );
  }
}
