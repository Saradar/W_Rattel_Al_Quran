import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../constants/MainTheme.dart';

class CustomOptionCard extends StatelessWidget {
  final String text1 ;
  final String text2 ;
  final String image;
  final Function onClick;
  const CustomOptionCard({Key? key,required this.text1,required this.text2 ,required this.image,required this.onClick}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return GestureDetector(
      child: Container(
        height: height * 0.3,
        width: 0.88 * width,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(20)),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.5),
                spreadRadius: 2,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ]),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                width: 15,
              ),
              Container(
                child: SvgPicture.asset(image),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    text1,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 25,
                      color: MainTheme.primaryColor,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                  Text(
                    text2,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 25,
                      color: MainTheme.primaryColor,
                      fontWeight: FontWeight.w800,
                    ),
                  ),

                ],
              )
            ],
          ),
        ),
      ),

    );
  }
}
