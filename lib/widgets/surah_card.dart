import 'package:flutter/material.dart';
import 'package:hivetemplate/constants/MainTheme.dart';

class SurahCard extends StatelessWidget {
  final String name;
  final int page;
  final Function onClick;
  final int numberofpages;
  const SurahCard(
      {Key? key, required this.name, required this.page,required this.numberofpages, required this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return GestureDetector(
      child: Center(
        child: Container(
          height: 70,
          width: 0.88 * width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Center(
            child: Row(
              children: [
                SizedBox(
                  width: 25,
                ),
                Column(
                  children: [
                    SizedBox(
                      height: 7,
                    ),
                    Text(numberofpages.toString(),style:TextStyle(fontSize: 20 ,color: MainTheme.primaryColor,fontWeight: FontWeight.w800),),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'صفحة',
                      textAlign: TextAlign.right,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w800,
                          color: MainTheme.primaryColor),
                    ),
                  ],
                ),
                SizedBox(width: width*0.45,),
                Text(name,textAlign: TextAlign.right,
                  style: TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.w800,
                      color: MainTheme.primaryColor),)
              ],
            ),
          ),
        ),
      ),
      onTap: onClick(),
    );
  }
}
