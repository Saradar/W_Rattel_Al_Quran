import 'package:hive/hive.dart';
import 'dart:convert';

import 'package:hivetemplate/controller/quran_controller.dart';
part 'ayah.g.dart';
@HiveType(typeId: 0)
class Ayah extends HiveObject {
  @HiveField(0)
  final String? text;
  @HiveField(1)
  final int? number;
  @HiveField(2)
  final int? globalId;
  @HiveField(3)
  final int? page;
  @HiveField(4)
  final int? juz;
  @HiveField(5)
  String? surahName;


  Ayah({this.number, this.text ,this.globalId,this.page,this.juz,this.surahName });
  factory Ayah.fromJSON(Map<String, dynamic> json) {
    String fixedText = QuranAPI.removePunicatation(json['text']);
    return Ayah(text: fixedText, number: json['numberInSurah'],globalId: json['number'],page: json['page'],juz:json['juz']);
  }

  Ayah copyWith({
    String? text,
    int? number,
    int? globalId,
    int? page,
    int? juz
  }) {
    return Ayah(
      text: text ?? this.text,
      number: number ?? this.number,
      globalId: globalId ?? this.globalId,
      page: page ?? this.page,
      juz: juz ?? this.juz,
    );
  }

  Ayah merge(Ayah model) {
    return Ayah(
      text: model.text ?? text,
      number: model.number ?? number,
      globalId: model.globalId ?? globalId,
      page:model.page ?? page,
      juz: model.juz ?? juz
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'text': text,
      'number': number,

    };
  }

  String toJson() => json.encode(toMap());

  factory Ayah.fromMap(Map<String, dynamic> map) {
    return Ayah(
      text: map['text'],
      number: map['number'],
    );
  }

  factory Ayah.fromJson(String source) => Ayah.fromMap(json.decode(source));

  @override
  String toString() => 'Ayah(text: $text, number: $number  )';
}
