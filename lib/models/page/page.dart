import 'package:hivetemplate/models/ayah/ayah.dart';
import 'dart:convert';
import 'dart:developer' as developer;

import '../new_ayah/new_ayah.dart';

class Page {
  final int? num;
  final List<NewAyah>? ayat;

  Page({this.num, this.ayat});

  factory Page.fromJSON(Map<String, dynamic> json) {
    Iterable ayahs = json['data']['ayahs'];
    List<NewAyah> ayahsList = ayahs.map((e) => NewAyah.fromJSON((e))).toList();
    return Page(num: json['data']['number'], ayat: ayahsList);

  }


  @override
  String toString() {
    return 'Surah(number: $num,  ayat: $ayat)';
  }

  /*Map<String, dynamic> toMap() {
    return {
      'number': num,
      'ayahs': ayat?.map((x) => x.toMap()).toList(),
    };
  }*/

  /*factory Page.fromMap(Map<String, dynamic> map) {
    return Page(
      num: map['number'],
      ayat: List<Ayah>.from(map['ayahs']?.map((x) => Ayah?.fromMap(x))),
    );
  }*/

  Page merge(Page model) {
    return Page(
      num: model.num ?? num,
      ayat: model.ayat ?? ayat,
    );
  }

  //String toJson() => json.encode(toMap());
  //factory Page.fromJson(String source) => Page.fromMap(json.decode(source));
}
