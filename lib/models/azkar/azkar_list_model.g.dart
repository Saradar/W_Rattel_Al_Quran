// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'azkar_list_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AzkarListModelAdapter extends TypeAdapter<AzkarListModel> {
  @override
  final int typeId = 7;

  @override
  AzkarListModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AzkarListModel(
      azkarList: (fields[0] as List?)?.cast<AzkarModel>(),
    );
  }

  @override
  void write(BinaryWriter writer, AzkarListModel obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.azkarList);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AzkarListModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
