import 'dart:convert';
import 'package:hive/hive.dart';

import 'azkar_model.dart';
part 'azkar_list_model.g.dart';
@HiveType(typeId: 7)
class AzkarListModel extends HiveObject{
  @HiveField(0)
  List<AzkarModel>? azkarList;
  AzkarListModel({this.azkarList});
}