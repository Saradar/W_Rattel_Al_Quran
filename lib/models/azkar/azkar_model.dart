import 'dart:convert';
import 'package:hive/hive.dart';
part 'azkar_model.g.dart';
@HiveType(typeId: 6)
class AzkarModel extends HiveObject {
  @HiveField(0)
  final String? name ;
  @HiveField(1)
  int counter ;
  AzkarModel({this .name ,required this.counter});

  void updateCounter(int newValue){
    counter = newValue;
  }
}