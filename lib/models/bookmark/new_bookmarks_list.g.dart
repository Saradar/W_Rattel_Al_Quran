// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_bookmarks_list.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BookmarkListModelAdapter extends TypeAdapter<BookmarkListModel> {
  @override
  final int typeId = 11;

  @override
  BookmarkListModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BookmarkListModel(
      bookmarks: (fields[0] as List).cast<dynamic>(),
    );
  }

  @override
  void write(BinaryWriter writer, BookmarkListModel obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.bookmarks);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookmarkListModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
