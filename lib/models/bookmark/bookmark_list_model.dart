import 'bookmark_model.dart';
import 'package:hive/hive.dart';

part 'bookmark_list_model.g.dart';
@HiveType(typeId: 4)
class BookmarkList extends HiveObject{

  @HiveField(0)
  final List<Bookmark>? bookmarkList;

  BookmarkList({this.bookmarkList});



}