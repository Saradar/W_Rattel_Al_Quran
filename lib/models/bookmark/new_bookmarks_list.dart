import 'package:hive/hive.dart';
part 'new_bookmarks_list.g.dart';
@HiveType(typeId: 11)
class BookmarkListModel extends HiveObject{
  @HiveField(0)
  List bookmarks = List.generate(625, (index) => false);

  BookmarkListModel({required this.bookmarks});
}