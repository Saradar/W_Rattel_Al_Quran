// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bookmark_list_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BookmarkListAdapter extends TypeAdapter<BookmarkList> {
  @override
  final int typeId = 4;

  @override
  BookmarkList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BookmarkList(
      bookmarkList: (fields[0] as List?)?.cast<Bookmark>(),
    );
  }

  @override
  void write(BinaryWriter writer, BookmarkList obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.bookmarkList);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookmarkListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
