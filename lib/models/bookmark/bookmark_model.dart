import 'package:hive/hive.dart';
import 'dart:convert';

part 'bookmark_model.g.dart';
@HiveType(typeId: 3)
class Bookmark extends HiveObject{
  @HiveField(0)
  final int? pageNumber;

  Bookmark({this.pageNumber });

}