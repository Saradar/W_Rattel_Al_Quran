import 'package:hive/hive.dart';
part 'quran_versions_model.g.dart';
@HiveType(typeId: 9)
class QuranVersions extends HiveObject{
  @HiveField(0)
  List<bool> quranVersionsList = [true,false,false];
  @HiveField(1)
  int currentVersionIndex = 0;

  QuranVersions({required this.quranVersionsList ,required this.currentVersionIndex});

}