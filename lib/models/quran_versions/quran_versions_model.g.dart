// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quran_versions_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class QuranVersionsAdapter extends TypeAdapter<QuranVersions> {
  @override
  final int typeId = 9;

  @override
  QuranVersions read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return QuranVersions(
      quranVersionsList: (fields[0] as List).cast<bool>(),
      currentVersionIndex: fields[1] as int,
    );
  }

  @override
  void write(BinaryWriter writer, QuranVersions obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.quranVersionsList)
      ..writeByte(1)
      ..write(obj.currentVersionIndex);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is QuranVersionsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
