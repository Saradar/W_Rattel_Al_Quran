import 'package:hive/hive.dart';
import 'dart:convert';
class NewAyah{
  final int? globalId ;
  final int? surahId;
  final int? id;
  final String? text;
  final int? pageId;


  NewAyah({this.globalId, this.surahId,this.id,this.text ,this.pageId});

  factory NewAyah.fromJSON(Map<String,dynamic>json){
    return NewAyah(globalId: json['number'],surahId: json['surah']['number'],id:json['numberInSurah'],text: json['text'],pageId:json['page']);
  }

}
