import 'package:hive/hive.dart';
part 'audio_model.g.dart';
@HiveType(typeId: 8)
class AudioModel extends HiveObject{
  @HiveField(0)
   List audioFiles = List.generate(6, (_) => List.generate(114, (_) => false));

  AudioModel({required this.audioFiles});
}