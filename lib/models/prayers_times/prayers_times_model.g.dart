// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prayers_times_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PrayersTimesModelAdapter extends TypeAdapter<PrayersTimesModel> {
  @override
  final int typeId = 1;

  @override
  PrayersTimesModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PrayersTimesModel(
      fajar: fields[0] as String?,
      sunrise: fields[1] as String?,
      duhur: fields[2] as String?,
      asr: fields[3] as String?,
      sunset: fields[4] as String?,
      maghrib: fields[5] as String?,
      ishaa: fields[6] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, PrayersTimesModel obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.fajar)
      ..writeByte(1)
      ..write(obj.sunrise)
      ..writeByte(2)
      ..write(obj.duhur)
      ..writeByte(3)
      ..write(obj.asr)
      ..writeByte(4)
      ..write(obj.sunset)
      ..writeByte(5)
      ..write(obj.maghrib)
      ..writeByte(6)
      ..write(obj.ishaa);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PrayersTimesModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
