// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prayers_timezone_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PrayersTimezoneAdapter extends TypeAdapter<PrayersTimezone> {
  @override
  final int typeId = 10;

  @override
  PrayersTimezone read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PrayersTimezone(
      country: fields[0] as String,
      capital: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, PrayersTimezone obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.country)
      ..writeByte(1)
      ..write(obj.capital);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PrayersTimezoneAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
