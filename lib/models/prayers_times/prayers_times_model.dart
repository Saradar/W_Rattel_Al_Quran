import 'package:hive/hive.dart';
import 'dart:convert';

import 'package:hivetemplate/controller/prayers_times_controller.dart';

part 'prayers_times_model.g.dart';

@HiveType(typeId: 1)
class PrayersTimesModel extends HiveObject{
  @HiveField(0)
  final String? fajar ;

  @HiveField(1)
  final String? sunrise;

  @HiveField(2)
  final String? duhur;

  @HiveField(3)
  final String? asr ;

  @HiveField(4)
  final String? sunset;

  @HiveField(5)
  final String? maghrib;

  @HiveField(6)
  final String? ishaa;

  PrayersTimesModel({this.fajar,this.sunrise,this.duhur,this.asr,this.sunset,this.maghrib,this.ishaa});

  factory PrayersTimesModel.fromJSON(Map<String, dynamic> json){
    return PrayersTimesModel(
      fajar: PrayersTimesController.FixTheTime(json['data']['timings']['Fajr']),
      sunrise: PrayersTimesController.FixTheTime(json['data']['timings']['Sunrise']),
      duhur: PrayersTimesController.FixTheTime(json['data']['timings']['Dhuhr']),
      asr: PrayersTimesController.FixTheTime(json['data']['timings']['Asr']),
      sunset: PrayersTimesController.FixTheTime(json['data']['timings']['Sunset']),
      maghrib:PrayersTimesController.FixTheTime( json['data']['timings']['Maghrib']),
      ishaa: PrayersTimesController.FixTheTime(json['data']['timings']['Isha'])
    );
    // return PrayersTimesModel(
    //     fajar: json['results']['datetime']["times"]['Fajr'],
    //     sunrise: json['results']['datetime']["times"]['Sunrise'],
    //     duhur: json['results']['datetime']["times"]['Dhuhr'],
    //     asr: json['results']['datetime']["times"]['Asr'],
    //     sunset: json['results']['datetime']["times"]['Sunset'],
    //     maghrib: json['results']['datetime']["times"]['Maghrib'],
    //     ishaa: json['results']['datetime']["times"]['Isha']
    // );
  }

}