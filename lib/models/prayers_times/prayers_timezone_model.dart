import 'package:hive/hive.dart';
part 'prayers_timezone_model.g.dart';

@HiveType(typeId: 10)
class PrayersTimezone extends HiveObject {
  @HiveField(0)
  String country = 'سوريا';
  @HiveField(1)
  String capital = 'دمشق';

  PrayersTimezone({required this.country,required this.capital});
}
