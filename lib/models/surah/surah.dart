import 'dart:convert';

import 'package:hive/hive.dart';

import '../ayah/ayah.dart';
part 'surah.g.dart';

@HiveType(typeId: 2)
class Surah extends HiveObject{
  @HiveField(0)
final int? number ;
  @HiveField(1)
final String? name;
  @HiveField(2)
final List<Ayah>? ayat ;

Surah({this.number,this.name,this.ayat});

factory Surah.fromJSON(Map<String, dynamic> json) {
  Iterable ayahs = json['ayahs'];
  List<Ayah> ayahsList = ayahs.map((e) => Ayah.fromJSON((e))).toList();

  return Surah(
      name: json['name'],
      number: json['number'],
      ayat: ayahsList);
}

Surah copyWith({
  int? number,
  String? name,
  List<Ayah>? ayat,
}) {
  return Surah(
    number: number ?? this.number,
    name: name ?? this.name,
    ayat: ayat ?? this.ayat,
  );
}

Surah merge(Surah model) {
  return Surah(
    number: model.number ?? number,
    name: model.name ?? name,
    ayat: model.ayat ?? ayat,
  );
}

Map<String, dynamic> toMap() {
  return {
    'number': number,
    'name': name,
    'ayahs': ayat?.map((x) => x.toMap()).toList(),
  };
}

factory Surah.fromMap(Map<String, dynamic> map) {
  return Surah(
    number: map['number'],
    name: map['name'],
    ayat: List<Ayah>.from(map['ayahs']?.map((x) => Ayah?.fromMap(x))),
  );
}

String toJson() => json.encode(toMap());

factory Surah.fromJson(String source) => Surah.fromMap(json.decode(source));

@override
String toString() {
  return 'Surah(number: $number, name: $name, ayat: $ayat)';
}

}