import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hivetemplate/constants/MainTheme.dart';
import 'package:hivetemplate/constants/constant_data.dart';

import '../models/azkar/azkar_model.dart';

class SubhaView2 extends StatefulWidget {
  const SubhaView2({Key? key}) : super(key: key);

  @override
  _SubhaView2State createState() => _SubhaView2State();
}

class _SubhaView2State extends State<SubhaView2> {
  int counter = 0;
  final List<String> items = ['سبحان الله','الحمدلله','لا إله إلا الله','الله أكبر','أستغفر الله ','الصلاة على النبي'];
  String selectedItem = 'سبحان الله';
  @override
  void initState() {
    // TODO: implement initState

    counter =  ConstantData.azkarList[0].counter;
    super.initState();
  }
  void IncreaseCounter() {
    setState(() {
      counter++;
    });
  }

  void ChangeTheSelectedItem(String value) {
    setState(() {
      selectedItem = value;
    });
  }

  void RestartTheCounter() {
    setState(() {
      counter = 0;
    });
  }
  void LoadCounter(String name){
    setState(() {
      if(name ==items[0])
        counter =  ConstantData.azkarList[0].counter;
      else if(name ==items[1])
        counter = ConstantData.azkarList[1].counter;
      else if(name ==items[2])
        counter = ConstantData.azkarList[2].counter;
      else if(name ==items[3])
        counter = ConstantData.azkarList[3].counter;
      else if(name ==items[4])
        counter = ConstantData.azkarList[4].counter;
      else if(name ==items[5])
        counter = ConstantData.azkarList[5].counter;
    });
  }
  void updateAzkarData(String name){
    setState(() {
      if(name ==items[0])
        ConstantData.azkarList[0].counter = counter;
      else if(name ==items[1])
        ConstantData.azkarList[1].counter = counter;
      else if(name ==items[2])
        ConstantData.azkarList[2].counter = counter;
      else if(name ==items[3])
        ConstantData.azkarList[3].counter = counter;
      else if(name ==items[4])
        ConstantData.azkarList[4].counter = counter;
      else if(name ==items[5])
        ConstantData.azkarList[5].counter = counter;
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: MainTheme.primaryColor,
        title: Text(
          'السبحة',
          textAlign: TextAlign.right,
          style: TextStyle(
              fontWeight: FontWeight.w800, fontSize: 18, color: Colors.white),
        ),
        automaticallyImplyLeading: true,
      ),
      body: Stack(
        children: [
          Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                /*SizedBox(height: 35,),
                SizedBox(height: height*0.10),*/
                SvgPicture.asset(MainTheme.ArabicDesign2)
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [

              Padding(
                padding: const EdgeInsets.only(top: 70),
                child: Center(
                  child: Container(
                    child: Center(
                      child: ElevatedButton(
                        child: Text(counter.toString(),style: TextStyle(color: Colors.white,fontSize: 40,fontWeight: FontWeight.w800),),
                        style:  ElevatedButton.styleFrom(
                            fixedSize: const Size(256, 256), shape: const CircleBorder(),primary: MainTheme.secondyColor),
                        onPressed: (){
                          IncreaseCounter();
                        },

                      ),
                    ),
                  )
                ),
              ),
            ],
          ),
          Padding(
            padding:  EdgeInsets.only(top:height*0.5),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: DropdownButton<String>(
                      value: selectedItem,
                      items: items.map(buildMenuItem).toList(),
                      onChanged: (value) {
                        ChangeTheSelectedItem(value!);
                        LoadCounter(value);
                      }),
                ),

                Center(
                  child: ElevatedButton(
                    child: Icon(
                      Icons.restart_alt_rounded,
                      color: Colors.white,
                    ),
                    style: ElevatedButton.styleFrom(
                        primary: MainTheme.primaryColor,
                        fixedSize: const Size(56,56),
                        shape:  const CircleBorder()
                    ),
                    onPressed: (){
                      RestartTheCounter();
                    },
                  ),
                ),
                Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ElevatedButton(
                          child: Icon(
                            Icons.history_toggle_off,
                            color: Colors.white,
                          ),
                          style: ElevatedButton.styleFrom(
                              primary: MainTheme.primaryColor,
                              fixedSize: const Size(56,56),
                              shape:  const CircleBorder()
                          ),
                          onPressed: (){
                            Navigator.pushNamed(context, '/subhaHistoryView');
                          },
                        ),
                        ElevatedButton(
                          child: Icon(
                            Icons.save,
                            color: Colors.white,
                          ),
                          style: ElevatedButton.styleFrom(
                              primary: MainTheme.primaryColor,
                              fixedSize: const Size(56,56),
                              shape:  const CircleBorder()
                          ),
                          onPressed: () async{
                            updateAzkarData(selectedItem);
                            await ConstantData.updateAzkar();
                          },
                        ),
                      ],
                    )
                )
              ],
            ),
          )
        ],
      ),
    );
  }
  DropdownMenuItem<String> buildMenuItem(String item) => DropdownMenuItem(
    value: item,
    child: Center(
      child: SizedBox(
        width: 200,
        child: Center(
          child: Text(
            item,
            style: TextStyle(fontWeight: FontWeight.w800, fontSize: 22,color: MainTheme.primaryColor,),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    ),
  );


}
