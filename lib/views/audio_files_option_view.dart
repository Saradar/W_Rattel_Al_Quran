import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hivetemplate/views/vocal_download_option.dart';
import 'package:hivetemplate/views/vocals_option_view.dart';

import '../constants/MainTheme.dart';
class AudioOptionView extends StatefulWidget {
  const AudioOptionView({Key? key}) : super(key: key);

  @override
  _AudioOptionViewState createState() => _AudioOptionViewState();
}

class _AudioOptionViewState extends State<AudioOptionView> {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MainTheme.primaryColor,
        centerTitle: true,
        automaticallyImplyLeading: true,
        title: Text(
          'القران الصوتي',
          textAlign: TextAlign.right,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w800,
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          iconSize: 20.0,
          onPressed: () {
            Navigator.pop(context);
          },
        ),

      ),
      body: VocalOptionView()
    );
  }
}
