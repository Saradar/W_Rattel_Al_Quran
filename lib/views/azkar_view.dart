import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


import '../constants/MainTheme.dart';
class AzkarView extends StatelessWidget {
  const AzkarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MainTheme.primaryColor,
        centerTitle: true,
        automaticallyImplyLeading: true,
        title: Text(
          'أذكار الصباح و المساء',
          textAlign: TextAlign.right,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w800,
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          iconSize: 20.0,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Stack(
        children: [
          Container(height: height,width: width,color: Colors.white,),
          Container(
            height: height * 0.40,
            width: width,
            decoration: BoxDecoration(
              color: MainTheme.primaryColor,
              borderRadius: const BorderRadius.only(bottomRight: Radius.circular(70)),
            ),
            child: SvgPicture.asset(MainTheme.ArabicDesign),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  child: Container(
                    height: height * 0.3,
                    width: 0.88 * width,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        boxShadow: [
                          BoxShadow(
                            color: MainTheme.primaryColor.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ]),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [

                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              child: SvgPicture.asset(MainTheme.dayDouaa),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'أذكار',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    fontSize: 25,
                                    color: MainTheme.primaryColor,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ),
                                Text(
                                  'الصباح',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    fontSize: 25,
                                    color: MainTheme.primaryColor,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ),

                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  onTap: (){
                    Navigator.pushNamed(context, '/azkarSabahListView');
                  }
                ),
                GestureDetector(
                    child: Container(
                      height: height * 0.3,
                      width: 0.88 * width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          boxShadow: [
                            BoxShadow(
                              color: MainTheme.primaryColor.withOpacity(0.5),
                              spreadRadius: 2,
                              blurRadius: 7,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ]),
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [

                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                child: SvgPicture.asset(MainTheme.nightDouaa),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'أذكار',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      fontSize: 25,
                                      color: MainTheme.primaryColor,
                                      fontWeight: FontWeight.w800,
                                    ),
                                  ),
                                  Text(
                                    'المساء',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      fontSize: 25,
                                      color: MainTheme.primaryColor,
                                      fontWeight: FontWeight.w800,
                                    ),
                                  ),

                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    onTap: (){
                      Navigator.pushNamed(context, '/azkarMassaaListView');
                    }
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
