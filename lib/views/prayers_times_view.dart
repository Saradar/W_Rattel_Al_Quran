import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:hivetemplate/constants/MainTheme.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:hivetemplate/controller/prayers_times_controller.dart';
import 'package:hivetemplate/models/prayers_times/prayers_times_model.dart';
import 'package:hivetemplate/widgets/next_prayer_time.dart';
import 'package:hivetemplate/widgets/prayers_times.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class PrayersTimesView extends StatefulWidget {
  const PrayersTimesView({Key? key}) : super(key: key);

  @override
  _PrayersTimesViewState createState() => _PrayersTimesViewState();
}



class _PrayersTimesViewState extends State<PrayersTimesView> {



  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MainTheme.primaryColor,
        title: Text(
          'أوقات الصلاة',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w800,
          ),
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: Stack(
        children: [
          Container(height: height,width: width,color: Colors.white,),
          Container(
            height: height * 0.55,
            width: width,
            decoration: BoxDecoration(
              color: MainTheme.primaryColor,
              borderRadius: const BorderRadius.only(bottomRight: Radius.circular(45)),

            ),
            child: Center(child: Image.asset(MainTheme.goldArabicDesign),),
          ),
          Center(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 120, 10, 0),
                  child: PrayersTimes(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }


}
