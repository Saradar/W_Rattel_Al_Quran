import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hivetemplate/constants/constant_data.dart';

import '../constants/MainTheme.dart';
import '../models/azkar/azkar_model.dart';
import 'dart:developer'as developer;

class SubhaHistory extends StatefulWidget {
  const SubhaHistory({Key? key}) : super(key: key);

  @override
  _SubhaHistoryState createState() => _SubhaHistoryState();
}

class _SubhaHistoryState extends State<SubhaHistory> {
  List<AzkarModel> azkarList = [
    AzkarModel(name: 'سبحان الله', counter: 0),
    AzkarModel(name: 'الحمدلله', counter: 0),
    AzkarModel(name: 'لا إله إلا الله', counter: 0),
    AzkarModel(name: 'الله أكبر', counter: 0),
    AzkarModel(name: 'أستغفر الله', counter: 0),
    AzkarModel(name: 'الصلاة على النبي', counter: 0),
  ];

  @override
  void initState() {
    // TODO: implement initState
    azkarList = ConstantData.azkarList;


    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: MainTheme.primaryColor,
        title: Text(
          'سجل السبحة',
          textAlign: TextAlign.right,
          style: TextStyle(
              fontWeight: FontWeight.w800, fontSize: 18, color: Colors.white),
        ),
        automaticallyImplyLeading: true,
      ),
      body: Stack(
        children: [
          Container(
            height: height,
            width: width,
            color: MainTheme.primaryColor,
            child: Center(child: Image.asset(MainTheme.goldArabicDesign)),
          ),
          Center(
            child: Container(
              height: height,
              width: width,
              child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height:120 ,
                              width: 150,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text('${azkarList[0].name}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                    Text('${azkarList[0].counter}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height:120 ,
                              width: 150,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text('${azkarList[1].name}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                    Text('${azkarList[1].counter}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height:120 ,
                              width: 150,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text('${azkarList[2].name}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                    Text('${azkarList[2].counter}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height:120 ,
                              width: 150,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text('${azkarList[3].name}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                    Text('${azkarList[3].counter}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height:120 ,
                              width: 150,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text('${azkarList[4].name}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                    Text('${azkarList[4].counter}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height:120 ,
                              width: 150,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text('${azkarList[5].name}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                    Text('${azkarList[5].counter}',style:MainTheme.mainTextStyle ,textAlign: TextAlign.center,),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );

  }
}
