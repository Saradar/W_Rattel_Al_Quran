
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:hivetemplate/models/bookmark/bookmark_model.dart';
import 'package:hivetemplate/views/quran_view.dart';
import 'dart:developer' as developer;

import '../constants/MainTheme.dart';
class BookmarkHistoryView extends StatefulWidget {
  const BookmarkHistoryView({Key? key}) : super(key: key);

  @override
  _BookmarkHistoryViewState createState() => _BookmarkHistoryViewState();
}

class _BookmarkHistoryViewState extends State<BookmarkHistoryView> {
  List<Bookmark>_bookmarks = [];
  @override
  void initState() {
    _getBookmarksHistory();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
          backgroundColor: MainTheme.primaryColor,
          centerTitle: true,
          automaticallyImplyLeading: true,
          title: Text(
            'سجل الراشدة',
            textAlign: TextAlign.right,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w800,
            ),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.delete_forever_rounded,
            ),
            onPressed: ()async{
              await ConstantData.clearHistory();

            },
          )

      ),
      body: Stack(
        children: [
          Container(
            height: height,
            width: width,
            color: Colors.white,
          ),
          Container(
            height: height * 0.55,
            width: width,
            decoration: BoxDecoration(
              color: MainTheme.primaryColor,
              borderRadius:
              const BorderRadius.only(bottomRight: Radius.circular(95)),
            ),
            child: Center(
              child: SvgPicture.asset(MainTheme.ArabicDesign2),
            ),
          ),

          FutureBuilder(
              future: _getBookmarksHistory(),
              builder: (context,AsyncSnapshot snapshot){
                if(!snapshot.hasData)
                  return Center(child: CircularProgressIndicator());
                else
                {
                  return ListView.separated(
                    shrinkWrap: true,
                    itemCount: _bookmarks.length,
                    padding: EdgeInsets.only(top: 10),
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        child: Center(
                          child: Container(
                            height: 70,
                            width: 0.88 * width,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(15)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('الصفحة :${621-(_bookmarks[index].pageNumber! - 1)}',style: MainTheme.mainTextStyle,textAlign: TextAlign.right,)
                                  ],
                                )),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => QuranView(intialPage:_bookmarks[index].pageNumber??621,versionNumber: ConstantData.currentQuranVersion,)),
                          );
                        },
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      return SizedBox(
                        height: 10,
                      );
                    },
                  );
                }
              }
          )



        ],
      ),
    );

  }
  Future<List<Bookmark>>_getBookmarksHistory() async{
    await ConstantData.intilizeBookmarks();
    setState(() {
      _bookmarks = ConstantData.bookmarks.toList();
    });

    return _bookmarks;
  }
}
