import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../constants/MainTheme.dart';
class AzkarListView2 extends StatefulWidget {
  const AzkarListView2({Key? key}) : super(key: key);

  @override
  _AzkarListView2State createState() => _AzkarListView2State();
}

class _AzkarListView2State extends State<AzkarListView2> {
  List<String> douaaNames = [
    '..أَمْسَيْـنا وَأَمْسـى المـلكُ لله',
    '..اللهـمَ أَنْتَ رَبِـي',
    '..رَضيـتُ بِاللهِ رَبَـاً',
    '..اللهُـمَ إِنِـي أَمسيتُ أُشْـهِدُك',
    '..اللهُـمَ ما أَمسى بي',
    '..حَسْبِـيَ اللهُ',
    '..بِسـمِ اللهِ الذي لا يَضُـرُ',
    '..اللهُـمَ بِكَ أَمْسَـينا',
    '..أَمْسَيْنَا عَلَى فِطْرَةِ الإسْلاَمِ',
    '..سُبْحـانَ اللهِ وَبِحَمْـدِهِ',
  ];

  List<String> douaaTexts = [
    'أَمْسَيْـنا وَأَمْسـى المـلكُ لله وَالحَمدُ لله ، لا إلهَ إلا اللهُ وَحدَهُ لا شَريكَ لهُ، لهُ المُـلكُ ولهُ الحَمْـد، وهُوَ على كل شَيءٍ قدير ، رَبِ أسْـأَلُـكَ خَـيرَ ما في هـذهِ اللَـيْلَةِ وَخَـيرَ ما بَعْـدَهـا ، وَأَعـوذُ بِكَ مِنْ شَـرِ ما في هـذهِ اللَـيْلةِ وَشَرِ ما بَعْـدَهـا ، رَبِ أَعـوذُبِكَ مِنَ الْكَسَـلِ وَسـوءِ الْكِـبَر ، رَبِ أَعـوذُ بِكَ مِنْ عَـذابٍ في النـارِ وَعَـذابٍ في القَـبْر',
    'اللهـمَ أَنْتَ رَبِـي لا إلهَ إلا أَنْتَ ، خَلَقْتَنـي وَأَنا عَبْـدُك ، وَأَنا عَلـى عَهْـدِكَ وَوَعْـدِكَ ما اسْتَـطَعْـت ، أَعـوذُبِكَ مِنْ شَـرِ ما صَنَـعْت ، أَبـوءُ لَـكَ بِنِعْـمَتِـكَ عَلَـيَ وَأَبـوءُ بِذَنْـبي فَاغْفـِرْ لي فَإِنَـهُ لا يَغْـفِرُ الذُنـوبَ إِلا أَنْتَ ',
    'رَضيـتُ بِاللهِ رَبَـاً وَبِالإسْلامِ ديـناً وَبِمُحَـمَدٍ صلى الله عليه وسلم نَبِيـاً',
    'اللهُـمَ إِنِـي أَمسيتُ أُشْـهِدُك ، وَأُشْـهِدُ حَمَلَـةَ عَـرْشِـك ، وَمَلَائِكَتَكَ ، وَجَمـيعَ خَلْـقِك ، أَنَـكَ أَنْـتَ اللهُ لا إلهَ إلا أَنْـتَ وَحْـدَكَ لا شَريكَ لَـك ، وَأَنَ ُ مُحَمـداً عَبْـدُكَ وَرَسـولُـك',
    'اللهُـمَ ما أَمسى بي مِـنْ نِعْـمَةٍ أَو بِأَحَـدٍ مِـنْ خَلْـقِك ، فَمِـنْكَ وَحْـدَكَ لا شريكَ لَـك ، فَلَـكَ الْحَمْـدُ وَلَـكَ الشُكْـر',
    'حَسْبِـيَ اللهُ لا إلهَ إلا هُوَ عَلَـيهِ تَوَكَـلتُ وَهُوَ رَبُ العَرْشِ العَظـيم',
    'بِسـمِ اللهِ الذي لا يَضُـرُ مَعَ اسمِـهِ شَيءٌ في الأرْضِ وَلا في السمـاءِ وَهـوَ السمـيعُ العَلـيم',
    'اللهُـمَ بِكَ أَمْسَـينا وَبِكَ أَصْـبَحْنا، وَبِكَ نَحْـيا وَبِكَ نَمُـوتُ وَإِلَـيْكَ الْمَصِيرُ',
    'أَمْسَيْنَا عَلَى فِطْرَةِ الإسْلاَمِ، وَعَلَى كَلِمَةِ الإِخْلاَصِ، وَعَلَى دِينِ نَبِيِنَا مُحَمَدٍ صَلَى اللهُ عَلَيْهِ وَسَلَمَ، وَعَلَى مِلَةِ أَبِينَا إبْرَاهِيمَ حَنِيفاً مُسْلِماً وَمَا كَانَ مِنَ المُشْرِكِينَ',
    'سُبْحـانَ اللهِ وَبِحَمْـدِهِ عَدَدَ خَلْـقِه ، وَرِضـا نَفْسِـه ، وَزِنَـةَ عَـرْشِـه ، وَمِـدادَ كَلِمـاتِـه ',
  ];
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MainTheme.primaryColor,
        centerTitle: true,
        automaticallyImplyLeading: true,
        title: Text(
          'اذكار المساء',
          textAlign: TextAlign.right,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w800,
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          iconSize: 20.0,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Stack(
        children: [
          Container(
            height: height,
            width: width,
            color: Colors.white,
          ),
          Container(
            height: height * 0.55,
            width: width,
            decoration: BoxDecoration(
              color: MainTheme.primaryColor,
              borderRadius:
              const BorderRadius.only(bottomRight: Radius.circular(95)),
            ),
            child: Center(
              child: SvgPicture.asset(MainTheme.ArabicDesign2),
            ),
          ),
          ListView.separated(
            shrinkWrap: true,
            itemCount: douaaNames.length,
            padding: EdgeInsets.only(top: 10),
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                child: Center(
                  child: Container(
                    height: 70,
                    width: 0.88 * width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(16),
                              child: Text(
                                douaaNames[index],
                                style: TextStyle(
                                    fontSize: 19,
                                    fontWeight: FontWeight.w800,
                                    color: MainTheme.primaryColor),
                                textAlign: TextAlign.right,
                              ),
                            )
                          ],
                        )),
                  ),
                ),
                onTap: () {
                  showDouaa(context,douaaTexts[index]);

                },
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                height: 10,
              );
            },
          )
        ],
      ),
    );
  }
  void showDouaa(BuildContext context,String text) {
    showDialog(context: context, builder: (BuildContext context) {
      return Dialog(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        child: SingleChildScrollView(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Text(
                text,
                style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 18,
                    color: Colors.black
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ),
        ),
      );
    });
  }
}
