import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:hivetemplate/controller/aladhan_controller.dart';
import 'package:hivetemplate/controller/connection_controller.dart';
import 'package:hivetemplate/controller/location_name_controller.dart';
import 'package:hivetemplate/controller/storage_service.dart';
import 'package:hivetemplate/utils/loader_dialog.dart';
import 'package:hivetemplate/views/about_us_view.dart';
import 'package:location/location.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../constants/MainTheme.dart';
class SettingsView2 extends StatefulWidget {
  const SettingsView2({Key? key}) : super(key: key);

  @override
  _SettingsView2State createState() => _SettingsView2State();
}

class _SettingsView2State extends State<SettingsView2> {
  List<bool> selectedQuranList = [false,false,false];
  double percenatge = 0.0;
  bool showDownload = false;
  Future<void>downloadQuran(int quranVersionIndex)async{
    final GlobalKey<NavigatorState> _loaderDialog =
    new GlobalKey<NavigatorState>();
    LoaderDialog.showLoadingDialog(context, _loaderDialog);
    Dio dio = Dio();
    try{
      String url;
      String name;
      if(quranVersionIndex == 1){
        url = await CloudStorage.downloadBlueQuranUrl();
      name = 'Blue.pdf';}
      else{
        url  = await CloudStorage.downloadBlackQuranUrl();
      name = 'Black.pdf';}

      Navigator.pop(context);
      if(url == null)
        {
          LoaderDialog.showTimeoutDialog(context);
          return;
        }

      setState(() {
        showDownload = true;
      });
      var response =await dio.download(url,"${ConstantData.dir.path}/quranVersions/$name",
          onReceiveProgress: (actualbytes,totalbytes){
            setState(() {
              var per = actualbytes/totalbytes*100;
              percenatge = (per/100) ;

            });
          },deleteOnError: true);

      if(response.statusCode==200){
        setState(() {
          showDownload = false;
        });
        LoaderDialog.showDoneDialog(context);
        ConstantData.quranVersionsList[quranVersionIndex] = true;
        await ConstantData.updateQuranVersions();

      }
      else{
        setState(() {
          showDownload = false;
        });
        LoaderDialog.showTimeoutDialog(context);
      }
    }
    catch(e){
      Navigator.pop(context);
      LoaderDialog.showTimeoutDialog(context);
    }
  }
  
  
  void initState(){

    selectedQuranList[ConstantData.currentQuranVersion-1] = true;
    
  }


  void changeTheSelectedQuran(int value)async{
    final GlobalKey<NavigatorState> _loaderDialog =
    new GlobalKey<NavigatorState>();
    LoaderDialog.showLoadingDialog(context,_loaderDialog );
    setState(() {
      selectedQuranList[ConstantData.currentQuranVersion-1] = false;
      ConstantData.currentQuranVersion = value;
      selectedQuranList[value-1] = true;
    });
    await ConstantData.updateQuranVersions();
    Navigator.pop(context);

  }
  

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: (){
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MainTheme.primaryColor,
          centerTitle: true,
          automaticallyImplyLeading: true,
          title: Text(
            'الاعدادات',
            textAlign: TextAlign.right,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w800,
            ),
          ),

        ),
        body: Stack(
          children: [
            Column(
              children: [
                /*Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 20, 5, 20),
                    child: Text('الموقع الحالي لأوقات الصلاة',style: MainTheme.mainTextStyle,),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 20, 5, 20),
                    child: Icon(Icons.location_pin,color: MainTheme.primaryColor,),

                  ),
                ],

              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DropdownButton<String>(
                    value: selectedItem,
                    items: items.map(buildMenuItem).toList(),
                    onChanged: (value) async{
                      if(await ConnectionController.hasInternetConnection())
                      changeTheSelectedItem(value!,context);
                      else
                        ConnectionController.showConnectionAlert(context);
                    }),
              ),*/
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 20, 5, 20),
                      child: Text('نسخة القران الحالية',style: MainTheme.mainTextStyle,),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 20, 5, 20),
                      child: Icon(Icons.format_color_reset_outlined,color: MainTheme.primaryColor,),

                    ),
                  ],

                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: GestureDetector(
                      child: Container(
                        height: 40,
                        width: width*0.88,
                        decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Center(
                          child: Visibility(
                            visible:selectedQuranList[0],
                            child: Icon(Icons.check,color: Colors.black,),
                          ),
                        ),
                      ),
                      onTap: (){
                        if(showDownload){
                          LoaderDialog.showDownloadWaitingDialog(context);
                        }
                       else{
                          setState(() {
                            changeTheSelectedQuran(1);
                          });
                        }
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: GestureDetector(
                      child: Container(
                        height: 40,
                        width: width*0.88,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),

                        child: Center(
                          child: Visibility(
                            visible:selectedQuranList[1],
                            child: Icon(Icons.check,color: Colors.black,),
                          ),
                        ),
                      ),
                      onTap: (){
                        if(showDownload){
                          LoaderDialog.showDownloadWaitingDialog(context);
                        }
                        else{
                        if(ConstantData.quranVersionsList[1])
                          setState(() {
                            changeTheSelectedQuran(2);
                          });
                        else
                          showAlertDialog(context,1);}
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: GestureDetector(
                      child: Container(
                        height: 40,
                        width: width*0.88,
                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Center(
                          child: Visibility(
                            visible:selectedQuranList[2],
                            child: Icon(Icons.check,color: Colors.white,),
                          ),
                        ),
                      ),
                      onTap: (){
                        if(showDownload){
                          LoaderDialog.showDownloadWaitingDialog(context);
                        }
                        else{
                        if(ConstantData.quranVersionsList[2])
                          setState(() {
                            changeTheSelectedQuran(3);
                          });
                        else
                          showAlertDialog(context,2);}
                      },
                    ),
                  ),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 20, 15, 20),
                      child: Text('حول التطبيق',style: MainTheme.mainTextStyle,),
                    ),
                    Icon(Icons.star,color: MainTheme.primaryColor,),

                  ],

                ),
                Center(
                  child: IconButton(icon: FaIcon(FontAwesomeIcons.question,color: MainTheme.primaryColor,),iconSize: 30,onPressed: (){
                    if(showDownload){
                      LoaderDialog.showDownloadWaitingDialog(context);
                    }

                    else {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => AboutUsView()),
                      );
                    }
                  },),
                ),

                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Visibility(
                    visible: showDownload,
                    child: Column(
                      children: [
                        Center(
                          child: Container(
                            height: 150,
                            width: width*0.8,
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Center(child: Text('يتم الان التنزيل...يرجى عدم الانتقال من الواجهة حتى انتهاء التحميل',style: MainTheme.mainTextStyle,textAlign: TextAlign.center,)),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Center(
                                      child: LinearProgressIndicator(
                                        color:MainTheme.primaryColor ,
                                        value: percenatge,
                                      ),
                                    ),
                                  ),
                                ),

                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )

              ],
            ),
          ],
        ) ,
      ),
    );
  }
  DropdownMenuItem<String> buildMenuItem(String country) => DropdownMenuItem(
    value: country,
    child: Container(
      height: 45,
      width: 300,
      decoration: BoxDecoration(
        color: MainTheme.secondyColor,
        borderRadius: BorderRadius.all(Radius.circular(15)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Center(
        child:Text(country,style: MainTheme.mainTextStyle,)
      ),
    )
  );
  showAlertDialog(BuildContext context,int quranVersionIndex) {

    // set up the button
    Widget okButton = TextButton(
      child: Text("تراجع" , style: MainTheme.mainTextStyle,),
      onPressed: ()  {
        Navigator.pop(context);
      },
    );
    Widget cancelButton = TextButton(
      child: Text("تنزيل",style: MainTheme.mainTextStyle,),
      onPressed: () async {
        if(await ConnectionController.hasInternetConnection()){

          Navigator.pop(context);
          await downloadQuran(quranVersionIndex);
        }
        else{Navigator.pop(context);
          ConnectionController.showConnectionAlert(context);}

      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Icon(Icons.warning_amber_outlined,size: 35,color: MainTheme.primaryColor,),
      content: Text("لم يتم تنزيل هذه النسخة بعد... هل ترغب في التنزيل ؟",textAlign:TextAlign.center,style: TextStyle(fontWeight: FontWeight.w700,fontSize: 20,color: MainTheme.primaryColor)),
      actions: [
        okButton,
        cancelButton
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showDownloadingDialog(BuildContext context){
    AlertDialog alertDialog = AlertDialog(
      title: Text('يتم الان التنزيل',textAlign:TextAlign.center,style: TextStyle(fontWeight: FontWeight.w700,fontSize: 20,color: MainTheme.primaryColor)),
      content: Container(
        child: Center(
          child: LinearProgressIndicator(
            color:MainTheme.primaryColor ,
            value: percenatge,
          ),
        ),
      ),
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alertDialog;
      },
    );

  }
}
