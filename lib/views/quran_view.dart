import 'dart:async';
import 'dart:io';

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:hivetemplate/constants/MainTheme.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:hivetemplate/controller/quran_controller.dart';
import 'package:hivetemplate/models/bookmark/bookmark_model.dart';
import 'package:hivetemplate/views/custom_search_view.dart';
import 'package:hivetemplate/widgets/custom_app_bar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdfx/pdfx.dart';
import 'package:search_page/search_page.dart';
import 'package:getwidget/getwidget.dart';
import 'dart:developer' as developer;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wakelock/wakelock.dart';

import '../models/ayah/ayah.dart';
import 'audio/playing_controls.dart';
import 'audio/postion_seek_widget.dart';

class QuranView extends StatefulWidget {
  final int intialPage;
  final int versionNumber;
  const QuranView(
      {Key? key, required this.intialPage, required this.versionNumber})
      : super(key: key);

  @override
  _QuranViewState createState() => _QuranViewState();
}

class _QuranViewState extends State<QuranView> {
  bool showAudioWidget = false;

  late final PdfController _pdfController;

  static int currentPage = 621;
  late AssetsAudioPlayer _assetsAudioPlayer;
  final List<StreamSubscription> _subscriptions = [];
  bool screenWakeOn = false;
  List bookmarks = List.generate(625, (index) => false);

  @override
  void dispose() {
    // TODO: implement dispose
    _pdfController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // _assetsAudioPlayer = AssetsAudioPlayer.newPlayer();
    // bookmarks = ConstantData.newBookmarks;
    // _subscriptions.add(_assetsAudioPlayer.playlistAudioFinished.listen((data) {
    //   print('playlistAudioFinished : $data');
    // }));
    // _subscriptions.add(_assetsAudioPlayer.audioSessionId.listen((sessionId) {
    //   print('audioSessionId : $sessionId');
    // }));
    // openPlayer();
    if (widget.versionNumber == 1) {
      _pdfController = PdfController(
        document: PdfDocument.openFile("${ConstantData.dir.path}/quranVersions/NewGreen.pdf"),

        initialPage: widget.intialPage,
      );
      currentPage = widget.intialPage;
    } else if (widget.versionNumber == 2) {
      _pdfController = PdfController(
        document: PdfDocument.openFile(
            '${ConstantData.dir.path}/quranVersions/Blue.pdf'),
        initialPage: widget.intialPage,
      );
      currentPage = widget.intialPage;
    } else {
      _pdfController = PdfController(
          document: PdfDocument.openFile(
              '${ConstantData.dir.path}/quranVersions/Black.pdf'),
          initialPage: widget.intialPage);
      currentPage = widget.intialPage;
    }

    // TODO: implement initState
    super.initState();
  }

  void openPlayer() async {
    await _assetsAudioPlayer.open(
      Playlist(audios: ConstantData.audios_0),
      showNotification: true,
      autoStart: false,
    );
  }

  Audio find(List<Audio> source, String fromPath) {
    return source.firstWhere((element) => element.path == fromPath);
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 50,
        backgroundColor: Colors.black,
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
              icon: ConstantData.newBookmarks[currentPage]
                  ? Icon(Icons.bookmark_remove)
                  : Icon(Icons.bookmark),
              iconSize: 25,
              onPressed: () {
                int page = currentPage;
                if (page > 621 || page < 18) page = 621;
                showAlertDialog(context, page);
              },
            ),
            // IconButton(
            //   icon: Icon(Icons.audiotrack),
            //   iconSize: 25,
            //   onPressed: () {
            //     setState(() {
            //       showAudioWidget = true;
            //     });
            //   },
            // ),
            IconButton(
              icon: screenWakeOn
                  ? Icon(Icons.remove_red_eye_outlined)
                  : Icon(Icons.remove_red_eye),
              iconSize: 25,
              onPressed: () {
                setState(() {
                  if (!screenWakeOn) {
                    Fluttertoast.showToast(
                        msg: "تم تفعيل وضع اليقظة",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.black,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                    Wakelock.enable();
                  } else {
                    Fluttertoast.showToast(
                        msg: "تم ايقاف وضع اليقظة",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.black,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                    Wakelock.disable();
                  }
                  screenWakeOn = !screenWakeOn;
                });
              },
            ),
            IconButton(
              icon: Icon(Icons.search),
              iconSize: 25,
              onPressed: () => showSearch(
                  context: context,
                  delegate: CustomSearchDelegate<Ayah>(
                    searchLabel: 'بحث',
                    searchStyle: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w800,
                    ),
                    barTheme: ThemeData(
                      primaryColor: MainTheme.primaryColor,
                      backgroundColor: MainTheme.primaryColor,
                    ),
                    items: QuranAPI.ayahs,
                    failure: Center(
                      child: Text("لا يوجد اية"),
                    ),
                    filter: (Ayah) => [
                      Ayah.text,
                    ],
                    builder: (Ayah) => Padding(
                      padding: const EdgeInsets.only(top:15),
                      child: Column(
                        children: [
                          GestureDetector(
                            child: Container(
                              height: 70,
                              width: width * 0.88,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      '${Ayah.number.toString()}الاية:',
                                      style: MainTheme.mainTextStyle,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(Ayah.surahName ?? '',
                                        style: MainTheme.mainTextStyle),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              int pageNum = 621 - (Ayah.page! - 1);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => QuranView(
                                          intialPage: pageNum,
                                          versionNumber:
                                              ConstantData.currentQuranVersion,
                                        )),
                              );
                            },
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  )),
            ),
          ],
        ),
      ),
      body: Stack(children: [
        Center(
            child: PdfView(
          controller: _pdfController,
          scrollDirection: Axis.horizontal,
          renderer: (PdfPage page) =>
              page.render(width: page.width*5, height: page.height*5,quality: 100,format:PdfPageImageFormat.png),
          onPageChanged: (int page) {
            developer.log(_pdfController.page.toString());

            setState(() {
              currentPage = _pdfController.page;
            });
          },
        )),
        // Visibility(
        //   visible: showAudioWidget,
        //   child: Center(
        //     child: Container(
        //       height: height * 0.45,
        //       width: width * 0.88,
        //       color: MainTheme.secondyColor,
        //       child: Padding(
        //         padding: const EdgeInsets.only(bottom: 48.0),
        //         child: Column(
        //           mainAxisSize: MainAxisSize.max,
        //           crossAxisAlignment: CrossAxisAlignment.stretch,
        //           children: <Widget>[
        //             StreamBuilder<Playing?>(
        //                 stream: _assetsAudioPlayer.current,
        //                 builder: (context, playing) {
        //                   if (playing.data != null) {
        //                     final myAudio = find(ConstantData.audios_0,
        //                         playing.data!.audio.assetAudioPath);
        //                     return Column(
        //                       children: [
        //                         Padding(
        //                           padding: const EdgeInsets.all(10.0),
        //                           child: Text(myAudio.metas.title.toString(),
        //                               style: TextStyle(
        //                                   fontSize: 20,
        //                                   fontWeight: FontWeight.w800,
        //                                   color: MainTheme.primaryColor),
        //                               textAlign: TextAlign.center),
        //                         ),
        //                         Padding(
        //                           padding: const EdgeInsets.all(10.0),
        //                           child: Text(
        //                             myAudio.metas.artist.toString(),
        //                             style: TextStyle(
        //                                 fontSize: 15,
        //                                 fontWeight: FontWeight.w800,
        //                                 color: MainTheme.primaryColor),
        //                             textAlign: TextAlign.center,
        //                           ),
        //                         ),
        //                       ],
        //                     );
        //                   }
        //                   return SizedBox.shrink();
        //                 }),
        //             _assetsAudioPlayer.builderCurrent(
        //                 builder: (context, Playing? playing) {
        //               return Column(
        //                 children: <Widget>[
        //                   _assetsAudioPlayer.builderLoopMode(
        //                     builder: (context, loopMode) {
        //                       return PlayerBuilder.isPlaying(
        //                           player: _assetsAudioPlayer,
        //                           builder: (context, isPlaying) {
        //                             return PlayingControls(
        //                               loopMode: loopMode,
        //                               isPlaying: isPlaying,
        //                               isPlaylist: true,
        //                               onStop: () {
        //                                 _assetsAudioPlayer.stop();
        //                               },
        //                               toggleLoop: () {
        //                                 _assetsAudioPlayer.toggleLoop();
        //                               },
        //                               onPlay: () {
        //                                 _assetsAudioPlayer.playOrPause();
        //                               },
        //                               onNext: () {
        //                                 //_assetsAudioPlayer.forward(Duration(seconds: 10));
        //                                 _assetsAudioPlayer.next();
        //                               },
        //                               onPrevious: () {
        //                                 _assetsAudioPlayer.previous();
        //                               },
        //                             );
        //                           });
        //                     },
        //                   ),
        //                   _assetsAudioPlayer.builderRealtimePlayingInfos(
        //                       builder: (context, RealtimePlayingInfos? infos) {
        //                     if (infos == null) {
        //                       return SizedBox();
        //                     }
        //                     //print('infos: $infos');
        //                     return Column(
        //                       children: [
        //                         PositionSeekWidget(
        //                           currentPosition: infos.currentPosition,
        //                           duration: infos.duration,
        //                           seekTo: (to) {
        //                             _assetsAudioPlayer.seek(to);
        //                           },
        //                         ),
        //                       ],
        //                     );
        //                   }),
        //                   Padding(
        //                     padding: const EdgeInsets.all(8.0),
        //                     child: IconButton(
        //                         onPressed: () {
        //                           setState(() {
        //                             showAudioWidget = false;
        //                           });
        //                         },
        //                         icon: Icon(Icons.cancel)),
        //                   )
        //                 ],
        //               );
        //             }),
        //           ],
        //         ),
        //       ),
        //     ),
        //   ),
        // )
      ]),
    );
  }

  showAlertDialog(BuildContext context, int pageNum) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("تم"),
      onPressed: () async {
        setState(() {
          if (ConstantData.newBookmarks[pageNum] == true) {
            ConstantData.newBookmarks[pageNum] = false;
            ConstantData.bookmarks.removeWhere((item) => item.pageNumber==pageNum);
          } else {
            Bookmark bookmark = Bookmark(pageNumber: pageNum);
            ConstantData.bookmarks.add(bookmark);
            ConstantData.newBookmarks[pageNum] = true;
          }
        });

        await ConstantData.updateBookmarks();
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "الراشدة",
        textAlign: TextAlign.center,
        style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
            color: MainTheme.primaryColor),
      ),
      content: Text("تم تحديث السجل ",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20,
              color: MainTheme.primaryColor)),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
