import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../constants/MainTheme.dart';

class AboutUsView extends StatelessWidget {
  const AboutUsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: MainTheme.primaryColor,
        title: Text(
          'حول التطبيق',
          textAlign: TextAlign.right,
          style: TextStyle(
              fontWeight: FontWeight.w800, fontSize: 18, color: Colors.white),
        ),
        automaticallyImplyLeading: true,
      ),
      body: Stack(
        children: [
          Container(
            height: height,
            width: width,
            color: MainTheme.primaryColor,
          ),
          Column(
            children: [
              Container(
                height: height * 0.40,
                width: width,
                decoration: BoxDecoration(
                  color: MainTheme.primaryColor,
                  borderRadius:
                      const BorderRadius.only(bottomRight: Radius.circular(70)),
                ),
                child: SvgPicture.asset(MainTheme.ArabicDesign),
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(top: 30,left: 20,right: 20),
                  child: Center(child: Image.asset(MainTheme.waqf)),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
