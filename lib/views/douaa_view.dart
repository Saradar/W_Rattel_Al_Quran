import 'package:flutter/material.dart';

import '../constants/MainTheme.dart';
class DouaaView extends StatelessWidget {
  final String title;
  final String text ;
  const DouaaView({Key? key,required this.title,required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MainTheme.primaryColor,
        centerTitle: true,
        automaticallyImplyLeading: true,
        title: Text(
          title,
          textAlign: TextAlign.right,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w800,
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          iconSize: 20.0,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        height: height ,
        width:  width,
        color: MainTheme.secondyColor,
        child: SingleChildScrollView(
          child:Text(
            'شَهِدَ اللّهُ أَنَّهُ لاَ إِلَـهَ إِلاَّ هُوَ وَالْمَلاَئِكَةُ وَأُوْلُواْ الْعِلْمِ قَآئِمًَا بِالْقِسْطِ لاَ إِلَـهَ إِلاَّ هُوَ الْعَزِيزُ الْحَكِيمُ إِنَّ الدِّينَ عِندَ اللّهِ الإِسْلاَمُ وَمَا اخْتَلَفَ الَّذِينَ أُوْتُواْ الْكِتَابَ إِلاَّ مِن بَعْدِ مَا جَاءهُمُ الْعِلْمُ بَغْيًا بَيْنَهُمْ وَمَن يَكْفُرْ بِآيَاتِ اللّهِ فَإِنَّ اللّهِ سَرِيعُ الْحِسَابِ',
            style: TextStyle(
              fontWeight: FontWeight.w800,
              fontSize: 18,
              color: Colors.black
            ),
          ),
        ),
      ),
    );
  }
}
