import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:hivetemplate/controller/connection_controller.dart';
import 'package:hivetemplate/controller/storage_service.dart';
import 'package:hivetemplate/utils/loader_dialog.dart';
import 'package:hivetemplate/views/audio/audioPlayer.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:developer' as developer;

import '../constants/MainTheme.dart';
import 'audio/online_audioPlayer.dart';

class SurahListAudioToDownload extends StatefulWidget {
  final int index;
  const SurahListAudioToDownload({Key? key, required this.index})
      : super(key: key);

  @override
  _SurahListAudioToDownloadState createState() =>
      _SurahListAudioToDownloadState();
}

class _SurahListAudioToDownloadState extends State<SurahListAudioToDownload> {
  List<bool> showProgressbar = List.generate(114, (index) => false);
  List<double> percenatge = List.generate(114, (index) => 0);
  List<bool> showDownloadButton = List.generate(114, (index) => true);
  List currentAudioIndex = List.generate(114, (index) => -1);
  List existSurah = [];

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }
  @override
  void initState() {
    // TODO: implement initState

    for (int i = 0; i < 114; i++) {
      showDownloadButton[i] = !ConstantData.audioFiles[widget.index][i];
    }
    if(widget.index == 0){
    currentAudioIndex = ConstantData.audioFileCurrentIndex0;
    existSurah = ConstantData.existSurahList0;
    }
    else if(widget.index ==1){
      currentAudioIndex = ConstantData.audioFileCurrentIndex1;
      existSurah = ConstantData.existSurahList1;}
    else if(widget.index ==2){
      currentAudioIndex = ConstantData.audioFileCurrentIndex2;
      existSurah = ConstantData.existSurahList2;
    }
    else if(widget.index ==3){
      currentAudioIndex = ConstantData.audioFileCurrentIndex3;
      existSurah = ConstantData.existSurahList3;
    }
    else if(widget.index ==4){
      currentAudioIndex = ConstantData.audioFileCurrentIndex4;
      existSurah = ConstantData.existSurahList4;}
    else if(widget.index ==5){
      currentAudioIndex = ConstantData.audioFileCurrentIndex5;
      existSurah = ConstantData.existSurahList5;
    }
    super.initState();




  }

  Future<void> downloadFile(int surahIndex) async {
    Dio dio = Dio();
    String url =
    await onlineAudioLink( surahIndex);
    if(url!=''){
      try {
        var dir = await getApplicationDocumentsDirectory();

        var response = await dio
            .download(url, "${dir.path}/${widget.index}/${surahIndex}.mp3",
            onReceiveProgress: (actualbytes, totalbytes) {
              setState(() {
                showDownloadButton[surahIndex] = false;
                showProgressbar[surahIndex] = true;
                var per = actualbytes / totalbytes * 100;
                percenatge[surahIndex] = (per / 100);
              });
            }, deleteOnError: true);
        if (response.statusCode == 200) {
          developer.log('Download Done');
          ConstantData.audioFiles[widget.index][surahIndex] = true;
          await ConstantData.updateDownloadAudioFiles();
          await ConstantData.initilizeAudioList();
          setState(() {
            if(widget.index == 0)
              currentAudioIndex = ConstantData.audioFileCurrentIndex0;
            else if(widget.index ==1)
              currentAudioIndex = ConstantData.audioFileCurrentIndex1;
            else if(widget.index ==2)
              currentAudioIndex = ConstantData.audioFileCurrentIndex2;
            else if(widget.index ==3)
              currentAudioIndex = ConstantData.audioFileCurrentIndex3;
            else if(widget.index ==4)
              currentAudioIndex = ConstantData.audioFileCurrentIndex4;
            else if(widget.index ==5)
              currentAudioIndex = ConstantData.audioFileCurrentIndex5;

            showProgressbar[surahIndex] = false;
          });
          LoaderDialog.showDoneDialog(context);
        }
      } catch (e) {
        LoaderDialog.showTimeoutDialog(context);
        developer.log(e.toString());
        setState(() {
          showDownloadButton[surahIndex] = true;
          showProgressbar[surahIndex] = false;
        });
      }
    }

  }

  Future<String>onlineAudioLink(int surahIndex)async{
    try {
      String url =
      await CloudStorage.downloadAudioFileUrl(widget.index, surahIndex);
      return url;
    }
    catch(e){
      LoaderDialog.showNotAvailableNowDialog(context);
      return '';
    }

  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () {
        bool isDownloading = false;
        for (int i = 0; i < showProgressbar.length; i++) {
          if (showProgressbar[i] == true) {
            isDownloading = true;
            break;
          }
        }
        if (isDownloading)
          showAlertDialog(context);
        else
          Navigator.pop(context);
        return Future.value(true);
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MainTheme.primaryColor,
          centerTitle: true,
          automaticallyImplyLeading: true,
          title: Text(
            ' المقرء :${ConstantData.vocals[widget.index]}  ',
            textAlign: TextAlign.right,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w800,
            ),
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            iconSize: 20.0,
            onPressed: () {
              bool isDownloading = false;
              for (int i = 0; i < showProgressbar.length; i++) {
                if (showProgressbar[i] == true) {
                  isDownloading = true;
                  break;
                }
              }
              if (isDownloading)
                showAlertDialog(context);
              else
                Navigator.pop(context);
            },
          ),
        ),
        body: Stack(
          children: [
            Container(
              height: height,
              width: width,
              color: Colors.white,
            ),
            Container(
              height: height * 0.55,
              width: width,
              decoration: BoxDecoration(
                color: MainTheme.primaryColor,
                borderRadius:
                    const BorderRadius.only(bottomRight: Radius.circular(95)),
              ),
              child: Center(
                child: SvgPicture.asset(MainTheme.ArabicDesign2),
              ),
            ),
            ListView.separated(
              shrinkWrap: true,
              itemCount: 114,
              padding: EdgeInsets.only(top: 8),
              itemBuilder: (BuildContext context, int index) {
                if(existSurah.contains(index+1))
                return GestureDetector(
                  child: Center(
                    child: Container(
                      height: 70,
                      width: 0.88 * width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Visibility(
                                  visible: showDownloadButton[index],
                                  child: IconButton(
                                      icon: Icon(
                                        Icons.download_sharp,
                                        color: Colors.black,
                                      ),
                                      onPressed: showDownloadButton[index]
                                          ? () async {
                                              final GlobalKey<NavigatorState>
                                                  _loaderDialog = new GlobalKey<
                                                      NavigatorState>();
                                              LoaderDialog.showLoadingDialog(
                                                  context, _loaderDialog);
                                              bool result =
                                                  await ConnectionController
                                                      .hasInternetConnection();
                                              Navigator.pop(context);
                                              developer.log(result.toString());
                                              if (!result) {
                                                ConnectionController
                                                    .showConnectionAlert(
                                                        context);
                                              } else {
                                                await downloadFile(index);
                                              }
                                            }
                                          : null),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    '  سورة :${ConstantData.surahsName[index]} ',
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: MainTheme.primaryColor,
                                        fontWeight: FontWeight.w800),
                                    textAlign: TextAlign.right,
                                  ),
                                ),
                              ],
                            ),
                            Visibility(
                              visible: showProgressbar[index],
                              child: Padding(
                                padding: const EdgeInsets.only(left: 10,right: 10,bottom: 5 ),
                                child: LinearProgressIndicator(
                                  value: percenatge[index],
                                  color: MainTheme.primaryColor,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  onTap: () async {
                    if (ConstantData.audioFiles[widget.index][index])
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => QuranAudioPLayer(
                                  vocalIndex: widget.index,
                                  surahIndex: currentAudioIndex[index],
                                )),
                      );
                    else {
                      final GlobalKey<NavigatorState>
                      _loaderDialog = new GlobalKey<
                          NavigatorState>();
                      LoaderDialog.showLoadingDialog(
                          context, _loaderDialog);
                      bool result =
                      await ConnectionController
                          .hasInternetConnection();
                      Navigator.pop(context);
                      developer.log(result.toString());
                      if (!result) {
                        ConnectionController
                            .showConnectionAlert(
                            context);
                      } else {
                        String link = await onlineAudioLink(index);
                        if(link!=''){
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => OnlineAudioPLayer(
                                  vocalIndex: widget.index,
                                  surahIndex: index,
                                  link: link,

                                )),
                          );
                        }
                      }
                    }
                  },
                );
                else {
                  return SizedBox(height: 0,);
                }
              },
              separatorBuilder: (BuildContext context, int index) {
                if(existSurah.contains(index+1))
                return SizedBox(
                  height: 10,
                );
                else
                  return SizedBox(height: 0,);
              },
            )
          ],
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text(
        "خروج",
        style: MainTheme.mainTextStyle,
      ),
      onPressed: () {
        Navigator.pop(context);
        Navigator.pop(context);
      },
    );
    Widget cancelButton = TextButton(
      child: Text(
        "تراجع",
        style: MainTheme.mainTextStyle,
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "تحذير",
        textAlign: TextAlign.center,
        style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 20,
            color: MainTheme.primaryColor),
      ),
      content: Text(
          "في حال كان هناك تحميل لم ينته .. سيتم اعادته من البداية في حال الخروج  ",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20,
              color: MainTheme.primaryColor)),
      actions: [okButton, cancelButton],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showNotDownloadedDialog(BuildContext context , int index) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: Container(
              height: 200,
              width: 300,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(15)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child:  Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.error,size: 30,color: MainTheme.primaryColor,),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Center(
                        child:Text('لم يتم التنزيل بعد',style: MainTheme.mainTextStyle,)
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextButton(onPressed: (){Navigator.pop(context);}, child: Text('تم',style: MainTheme.mainTextStyle,)),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextButton( child: Text('الاستماع اونلاين',style: MainTheme.mainTextStyle,),onPressed: ()async{
                            final GlobalKey<NavigatorState>
                            _loaderDialog = new GlobalKey<
                                NavigatorState>();
                            LoaderDialog.showLoadingDialog(
                                context, _loaderDialog);
                            bool result =
                            await ConnectionController
                                .hasInternetConnection();
                            Navigator.pop(context);
                            developer.log(result.toString());
                            if (!result) {
                              ConnectionController
                                  .showConnectionAlert(
                                  context);
                            } else {
                              String link = await onlineAudioLink(index);
                              if(link!=''){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => OnlineAudioPLayer(
                                        vocalIndex: widget.index,
                                        surahIndex: index,
                                        link: link,

                                      )),
                                );
                              }
                            }
                          },),
                        )
                      ],
                    ),
                  )
                ],
              )
          ),
        );
      },
    );


  }
}
