import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hivetemplate/utils/loader_dialog.dart';
import 'package:hivetemplate/views/surah_list_to_download.dart';

import '../constants/MainTheme.dart';
class VocalOptionView extends StatefulWidget {
  const VocalOptionView({Key? key }) : super(key: key);

  @override
  _VocalOptionViewState createState() => _VocalOptionViewState();
}

class _VocalOptionViewState extends State<VocalOptionView> {
  final List<String>vocals = [
    'بدر التركي',
    'عبدالله خياط',
    'عبدالرحمن مسعد',
    'اسلام صبحي',
    'عبدالعزيز العسيري',
    'رعد الكردي',
  ];
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return  Stack(
        children: [
          Container(
            height: height,
            width: width,
            color: Colors.white,
          ),
          Container(
            height: height * 0.55,
            width: width,
            decoration: BoxDecoration(
              color: MainTheme.primaryColor,
              borderRadius:
              const BorderRadius.only(bottomRight: Radius.circular(95)),
            ),
            child: Center(
              child: SvgPicture.asset(MainTheme.ArabicDesign2),
            ),
          ),
          ListView.separated(
            shrinkWrap: true,
            itemCount:vocals.length,
            padding: EdgeInsets.only(top: 8),
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                child: Center(
                  child: Container(
                    height: 70,
                    width: 0.88 * width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Center(
                      child: Text(vocals[index],style:TextStyle(fontSize: 20 ,color: MainTheme.primaryColor,fontWeight: FontWeight.w800),)
                    ),
                  ),
                ),
                onTap: (){

                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => SurahListAudioToDownload(index: index)));



                },
              );
            }, separatorBuilder: (BuildContext context, int index) {
            return SizedBox(height: 20,);
          },
          )
        ],

    );
  }
}
