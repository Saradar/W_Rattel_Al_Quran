import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hivetemplate/constants/MainTheme.dart';
import 'package:hivetemplate/views/surah_list_view2.dart';
import 'package:hivetemplate/widgets/home_view_card.dart';
class HomeView extends StatelessWidget {
  HomeView({Key? key}) : super(key: key);
  final List<String>_listTitels = ["القران الكريم ","أذكار","المسبحة","أوقات الصلاة", "القران الصوتي ","الرقية الشرعية "];
  final List<String>_listIcons = [MainTheme.homeQuran,MainTheme.homeIcon2,MainTheme.homeIcon3,MainTheme.homeIcon4,MainTheme.homeAudio,MainTheme.homeDouaa];
  final List<String>_listRoutes = ['/quranOptionsView','/azkarView','/subhaTemp','/prayersTimes','/audioOptionView','/rukiaView'];
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Stack(
      children: [
        Container(height: height,width: width,color: Colors.white,),
        Container(
          height: height * 0.55,
          width: width,
          decoration: BoxDecoration(
            color: MainTheme.primaryColor,
            borderRadius: const BorderRadius.only(bottomRight: Radius.circular(45)),

          ),
          child: SvgPicture.asset(MainTheme.ArabicDesign),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
          child: ListView.separated(
            itemBuilder: (BuildContext context, int index){
              return GestureDetector(
                child: Center(
                  child: Container(
                    height: 100,
                    width: 0.88 * width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              child: SvgPicture.asset(_listIcons[index]),
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text( _listTitels[index],
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w800,
                                    color: MainTheme.primaryColor)),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                onTap: (){
                  Navigator.pushNamed(context, _listRoutes[index]);
                },
              );
            },
            itemCount: 6, separatorBuilder: (BuildContext context, int index) {
              return SizedBox(height: 15,);
          },
          ),
        )
      ],
    );
  }
}
