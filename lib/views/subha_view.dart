import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hivetemplate/constants/MainTheme.dart';
import 'package:hivetemplate/constants/constant_data.dart';

import '../models/azkar/azkar_model.dart';

class SubhaView extends StatefulWidget {
  const SubhaView({Key? key}) : super(key: key);

  @override
  _SubhaViewState createState() => _SubhaViewState();
}

class _SubhaViewState extends State<SubhaView> {
  int counter = 0;
  final List<String> items = ['الحمدلله','استغفرالله','لا اله الا الله','سبحان الله','الله أكبر','صلاة على النبي'];
  String selectedItem = 'الحمدلله';
  @override
  void initState() {
    // TODO: implement initState

    counter =  ConstantData.azkarList[0].counter;
    super.initState();
  }
  void IncreaseCounter() {
    setState(() {
      counter++;
    });
  }

  void ChangeTheSelectedItem(String value) {
    setState(() {
      selectedItem = value;
    });
  }

  void RestartTheCounter() {
    setState(() {
      counter = 0;
    });
  }
  void LoadCounter(String name){
    setState(() {
      if(name =='الحمدلله')
        counter =  ConstantData.azkarList[0].counter;
      else if(name =='استغفرالله')
        counter = ConstantData.azkarList[1].counter;
      else if(name =='لا اله الا الله')
        counter = ConstantData.azkarList[2].counter;
      else if(name =='سبحان الله')
        counter = ConstantData.azkarList[3].counter;
      else if(name =='الله أكبر')
        counter = ConstantData.azkarList[4].counter;
      else if(name =='صلاة على النبي')
        counter = ConstantData.azkarList[5].counter;
    });
  }
  void updateAzkarData(String name){
    setState(() {
      if(name =='الحمدلله')
        ConstantData.azkarList[0].counter = counter;
      else if(name =='استغفرالله')
        ConstantData.azkarList[1].counter = counter;
      else if(name =='لا اله الا الله')
        ConstantData.azkarList[2].counter = counter;
      else if(name =='سبحان الله')
        ConstantData.azkarList[3].counter = counter;
      else if(name =='الله أكبر')
        ConstantData.azkarList[4].counter = counter;
      else if(name =='صلاة على النبي')
        ConstantData.azkarList[5].counter = counter;
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: MainTheme.primaryColor,
        title: Text(
          'السبحة',
          textAlign: TextAlign.right,
          style: TextStyle(
              fontWeight: FontWeight.w800, fontSize: 18, color: Colors.white),
        ),
        automaticallyImplyLeading: true,
      ),
      body: Stack(
        children: [
          Container(
            height: height,
            width: width,
            color: Colors.white,
            child: Center(
              child: SvgPicture.asset(MainTheme.ArabicDesign2),
            ),
          ),
          Column(
            children: [
              SizedBox(height: 10,),
              DropdownButton<String>(
                  value: selectedItem,
                  items: items.map(buildMenuItem).toList(),
                  onChanged: (value) {
                    ChangeTheSelectedItem(value!);
                    LoadCounter(value);
                  }),
              SizedBox(height: height*0.19,),
              Center(
                child:ElevatedButton(
                  child: Text(counter.toString(),style: TextStyle(color: Colors.white,fontSize: 40,fontWeight: FontWeight.w800),),
                  style:  ElevatedButton.styleFrom(
                      fixedSize: const Size(256, 256), shape: const CircleBorder(),primary: MainTheme.secondyColor),
                  onPressed: (){
                    IncreaseCounter();
                  },

                ),
              ),

              SizedBox(height: 60,),
              Center(
                child: ElevatedButton(
                  child: Icon(
                    Icons.restart_alt_rounded,
                    color: Colors.white,
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: MainTheme.primaryColor,
                    fixedSize: const Size(56,56),
                    shape:  const CircleBorder()
                  ),
                  onPressed: (){
                    RestartTheCounter();
                  },
                ),
              ),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      child: Icon(
                        Icons.history_toggle_off,
                        color: Colors.white,
                      ),
                      style: ElevatedButton.styleFrom(
                          primary: MainTheme.primaryColor,
                          fixedSize: const Size(56,56),
                          shape:  const CircleBorder()
                      ),
                      onPressed: (){
                        Navigator.pushNamed(context, '/subhaHistoryView');
                      },
                    ),
                    ElevatedButton(
                      child: Icon(
                        Icons.save,
                        color: Colors.white,
                      ),
                      style: ElevatedButton.styleFrom(
                          primary: MainTheme.primaryColor,
                          fixedSize: const Size(56,56),
                          shape:  const CircleBorder()
                      ),
                      onPressed: () async{
                        updateAzkarData(selectedItem);
                        await ConstantData.updateAzkar();
                      },
                    ),
                  ],
                )
              )




            ],
          )
        ],
      ),
    );
  }
  DropdownMenuItem<String> buildMenuItem(String item) => DropdownMenuItem(
    value: item,
    child: Text(
      item,
      style: TextStyle(fontWeight: FontWeight.w800, fontSize: 22,color: MainTheme.primaryColor),
    ),
  );


}
