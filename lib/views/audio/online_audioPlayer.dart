import 'dart:async';

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:hivetemplate/views/audio/playing_controls.dart';
import 'package:hivetemplate/views/audio/postion_seek_widget.dart';
import 'package:hivetemplate/views/audio/song_selecter.dart';

import '../../constants/MainTheme.dart';
import 'dart:developer'as developer;
class OnlineAudioPLayer extends StatefulWidget {
  final int vocalIndex;
  final int surahIndex;
  final String link;
  const OnlineAudioPLayer({Key? key , required this.vocalIndex,required this.surahIndex,required this.link}) : super(key: key);

  @override
  _OnlineAudioPLayerState createState() => _OnlineAudioPLayerState();
}

class _OnlineAudioPLayerState extends State<OnlineAudioPLayer> {

  final List<StreamSubscription> _subscriptions = [];
  List <Audio>audios = [];
  @override
  void initState() {
    audios = [Audio.network(
        '${widget.link}',
        metas: Metas(
            id: (widget.surahIndex).toString(),
            title: ConstantData.surahsName[widget.surahIndex],
            artist: ConstantData.vocals[widget.vocalIndex]
        )

    ),
      Audio.network(
          '${widget.link}',
          metas: Metas(
              id: (widget.surahIndex+1).toString(),
              title: ConstantData.surahsName[widget.surahIndex],
              artist: ConstantData.vocals[widget.vocalIndex]
          )

      ),];
    ConstantData.assetsAudioPlayer.stop();
    ConstantData.assetsAudioPlayer.dispose();
    ConstantData.assetsAudioPlayer = AssetsAudioPlayer.newPlayer();
    _subscriptions.add(ConstantData.assetsAudioPlayer.playlistAudioFinished.listen((data) {
      print('playlistAudioFinished : $data');
    }));
    _subscriptions.add(ConstantData.assetsAudioPlayer.audioSessionId.listen((sessionId) {
      print('audioSessionId : $sessionId');
    }));
    openPlayer();
    super.initState();
  }
  void openPlayer() async {

    await ConstantData.assetsAudioPlayer.open(
      Playlist(audios:audios , startIndex: widget.surahIndex),
      showNotification: true,
      autoStart: false,
      notificationSettings: NotificationSettings(
          seekBarEnabled: true,
          stopEnabled: true,
          customStopAction: (player){
            ConstantData.assetsAudioPlayer.pause();
            ConstantData.assetsAudioPlayer.seek(Duration(seconds: 0));
          }
        //prevEnabled: false,
        //customNextAction: (player) {
        //  print('next');
        //}
        //customStopIcon: AndroidResDrawable(name: 'ic_stop_custom'),
        //customPauseIcon: AndroidResDrawable(name:'ic_pause_custom'),
        //customPlayIcon: AndroidResDrawable(name:'ic_play_custom'),
      ),
    );
  }
  Audio find(List<Audio> source, String fromPath) {
    return source.firstWhere((element) => element.path == fromPath);
  }
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: MainTheme.primaryColor,
          title: Text(
            'مشغل القران الصوتي',
            textAlign: TextAlign.right,
            style: TextStyle(
                fontWeight: FontWeight.w800, fontSize: 18, color: Colors.white),
          ),
          automaticallyImplyLeading: true,
        ),
        body:Stack(
          children: [
            Container(height: height,width: width,color: Colors.white,),
            Column(
              children: [
                Container(
                  height: height * 0.40,
                  width: width,
                  decoration: BoxDecoration(
                    color: MainTheme.primaryColor,
                    //borderRadius: const BorderRadius.only(bottomRight: Radius.circular(70)),
                  ),
                  child: SvgPicture.asset(MainTheme.ArabicDesign),
                ),
                Expanded(
                  child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 48.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          SizedBox(
                            height: 20,
                          ),
                          StreamBuilder<Playing?>(
                              stream: ConstantData.assetsAudioPlayer.current,
                              builder: (context, playing) {
                                if(playing.data!=null){
                                  final myAudio = find(
                                      audios, playing.data!.audio.assetAudioPath);
                                  developer.log('here');
                                  return Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Text(myAudio.metas.title.toString(),style:TextStyle(fontSize: 20,fontWeight: FontWeight.w800,color: MainTheme.primaryColor),textAlign: TextAlign.center),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Text(myAudio.metas.artist.toString(),style: TextStyle(fontSize: 20,fontWeight: FontWeight.w800,color: MainTheme.primaryColor),textAlign: TextAlign.center,),
                                        ),
                                      ],
                                    ),
                                  );}
                                return SizedBox.shrink();
                              }),
                          ConstantData.assetsAudioPlayer.builderCurrent(
                              builder: (context, Playing? playing) {
                                return Column(
                                  children: <Widget>[
                                    ConstantData.assetsAudioPlayer.builderLoopMode(
                                      builder: (context, loopMode) {
                                        return PlayerBuilder.isPlaying(
                                            player: ConstantData.assetsAudioPlayer,
                                            builder: (context, isPlaying) {
                                              return PlayingControls(
                                                loopMode: loopMode,
                                                isPlaying: isPlaying,
                                                isPlaylist: true,
                                                onStop: () {
                                                  ConstantData.assetsAudioPlayer.pause();
                                                  ConstantData.assetsAudioPlayer.seek(Duration(seconds: 0));
                                                },
                                                toggleLoop: () {
                                                  ConstantData.assetsAudioPlayer.toggleLoop();
                                                },
                                                onPlay: () {
                                                  ConstantData.assetsAudioPlayer.playOrPause();
                                                },
                                                onNext: () {
                                                  //_assetsAudioPlayer.forward(Duration(seconds: 10));
                                                  ConstantData.assetsAudioPlayer.next();
                                                },
                                                onPrevious: () {
                                                  ConstantData.assetsAudioPlayer.previous();
                                                },
                                              );
                                            });
                                      },
                                    ),
                                    ConstantData.assetsAudioPlayer.builderRealtimePlayingInfos(
                                        builder: (context, RealtimePlayingInfos? infos) {
                                          if (infos == null) {
                                            return SizedBox();
                                          }
                                          //print('infos: $infos');
                                          return Column(
                                            children: [
                                              PositionSeekWidget(
                                                currentPosition: infos.currentPosition,
                                                duration: infos.duration,
                                                seekTo: (to) {
                                                  ConstantData.assetsAudioPlayer.seek(to);
                                                },
                                              ),
                                            ],
                                          );
                                        }),
                                  ],
                                );
                              }),
                          SizedBox(
                            height: 20,
                          ),
                          /* _assetsAudioPlayer.builderCurrent(
                            builder: (BuildContext context, Playing? playing) {
                              return SongsSelector(
                                audios: audios,
                                onPlaylistSelected: (myAudios) {
                                  _assetsAudioPlayer.open(
                                    Playlist(audios: myAudios),
                                    showNotification: true,
                                    headPhoneStrategy:
                                    HeadPhoneStrategy.pauseOnUnplugPlayOnPlug,
                                    audioFocusStrategy: AudioFocusStrategy.request(
                                        resumeAfterInterruption: true),
                                  );
                                },
                                onSelected: (myAudio) async {
                                  try {
                                    await _assetsAudioPlayer.open(
                                      myAudio,
                                      autoStart: false,
                                      showNotification: true,
                                      playInBackground: PlayInBackground.enabled,
                                      audioFocusStrategy: AudioFocusStrategy.request(
                                          resumeAfterInterruption: true,
                                          resumeOthersPlayersAfterDone: true),
                                      headPhoneStrategy: HeadPhoneStrategy.pauseOnUnplug,
                                      notificationSettings: NotificationSettings(
                                        //seekBarEnabled: false,
                                        //stopEnabled: true,
                                        //customStopAction: (player){
                                        //  player.stop();
                                        //}
                                        //prevEnabled: false,
                                        //customNextAction: (player) {
                                        //  print('next');
                                        //}
                                        //customStopIcon: AndroidResDrawable(name: 'ic_stop_custom'),
                                        //customPauseIcon: AndroidResDrawable(name:'ic_pause_custom'),
                                        //customPlayIcon: AndroidResDrawable(name:'ic_play_custom'),
                                      ),
                                    );
                                  } catch (e) {
                                    print(e);
                                  }
                                },
                                playing: playing,
                              );
                            }),*/

                        ],
                      ),
                    ),
                  ),
                ),

              ],
            )

          ],
        )
    );
  }
}
