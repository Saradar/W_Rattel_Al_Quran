import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:hivetemplate/views/juz_list_view.dart';
import 'package:hivetemplate/views/quran_view.dart';
import 'package:hivetemplate/views/surah_list_view2.dart';

import '../constants/MainTheme.dart';
class QuranOptionsView extends StatelessWidget {
  const QuranOptionsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return DefaultTabController(
    length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MainTheme.primaryColor,
          centerTitle: true,
          automaticallyImplyLeading: true,
          title: Text(
            'القران الكريم',
            textAlign: TextAlign.right,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w800,
            ),
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            iconSize: 20.0,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          bottom: TabBar(
            tabs: [
              Tab(text: 'سور القران',),
              Tab(text: 'أجزاء القران',),
            ],
          ),
        ),
        body:TabBarView(
          children: [
            SurahListView(),
            JuzListView(),
          ],
        )
      ),
    );
  }
}
