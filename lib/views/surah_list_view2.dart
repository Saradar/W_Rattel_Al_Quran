import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:hivetemplate/controller/quran_controller.dart';
import 'package:hivetemplate/models/ayah/ayah.dart';
import 'package:hivetemplate/views/quran_view.dart';
import 'dart:developer' as developer;

import '../constants/MainTheme.dart';
import '../widgets/surah_card.dart';

class SurahListView extends StatefulWidget {
  const SurahListView({Key? key}) : super(key: key);

  @override
  _SurahListViewState createState() => _SurahListViewState();
}

class _SurahListViewState extends State<SurahListView> {
   List<String> surahsName = [
    'الفاتحة',
    'البقرة',
    'ال عمران',
    'النِّسَاء',
    'المَائدة',
    'الأنعَام',
    'الأعرَاف',
    'الأنفَال',
    'التوبَة',
    'يُونس',
    'هُود',
    'يُوسُف',
    'الرَّعْد',
    'إبراهِيم',
    'الحِجْر',
    'النَّحْل',
    'الإسْرَاء',
    'الكهْف',
    'مَريَم',
    'طه',
    'الأنبيَاء',
    'الحَج',
    'المُؤمنون',
    'النُّور',
    'الفُرْقان',
    'الشُّعَرَاء',
    'النَّمْل',
    'القَصَص',
    'العَنكبوت',
    'الرُّوم',
    'لقمَان',
    'السَّجدَة',
    'الأحزَاب',
    'سَبَأ',
    'فَاطِر',
    'يس',
    'الصَّافات',
    'ص',
    'الزُّمَر',
    'غَافِر',
    'فُصِّلَتْ',
    'الشُّورَى',
    'الزُّخْرُف',
    'الدخَان',
    'الجَاثيَة',
    'الأحْقاف',
    'محَمَّد',
    'الفَتْح',
    'الحُجرَات',
    'ق',
    'الذَّاريَات',
    'الطُّور',
    'النَّجْم',
    'القَمَر',
    'الرَّحمن',
    'الوَاقِعَة',
    'الحَديد',
    'المجَادلة',
    'الحَشر',
    'المُمتَحنَة',
    'الصَّف',
    'الجُمُعَة',
    'المنَافِقون',
    'التغَابُن',
    'الطلَاق',
    'التحْريم',
    'المُلْك',
    'القَلَم',
    'الحَاقَّة',
    'المعَارج',
    'نُوح',
    'الجِن',
    'المُزَّمِّل',
    'المُدَّثِّر',
    'القِيَامَة',
    'الإنسَان',
    'المُرسَلات',
    'النَّبَأ',
    'النّازعَات',
    'عَبَس',
    'التَّكوير',
    'الانفِطار',
    'المطفِّفِين',
    'الانْشِقَاق',
    'البرُوج',
    'الطَّارِق',
    'الأَعْلى',
    'الغَاشِية',
    'الفَجْر',
    'البَلَد',
    'الشَّمْس',
    'الليْل',
    'الضُّحَى',
    'الشَّرْح',
    'التِّين',
    'العَلَق',
    'القَدْر',
    'البَينَة',
    'الزلزَلة',
    'العَادِيات',
    'القَارِعة',
    'التَّكَاثر',
    'العَصْر',
    'الهُمَزَة',
    'الفِيل',
    'قُرَيْش',
    'المَاعُون',
    'الكَوْثَر',
    'الكَافِرُون',
    'النَّصر',
    'المَسَد',
    'الإخْلَاص',
    'الفَلَق',
    'النَّاس',
  ];
  final List<int> surahsPageNumber = [
    1,
    2,
    50,
    77,
    106,
    128,
    151,
    177,
    187,
    208,
    221,
    235,
    249,
    255,
    262,
    267,
    282,
    293,
    305,
    312,
    322,
    332,
    342,
    350,
    359,
    367,
    377,
    385,
    396,
    404,
    411,
    415,
    418,
    428,
    434,
    440,
    446,
    453,
    458,
    467,
    477,
    483,
    489,
    496,
    499,
    502,
    507,
    511,
    515,
    518,
    520,
    523,
    526,
    528,
    531,
    534,
    537,
    542,
    545,
    549,
    551,
    553,
    554,
    556,
    558,
    560,
    562,
    564,
    566,
    568,
    570,
    572,
    574,
    575,
    577,
    578,
    580,
    582,
    583,
    585,
    586,
    587,
    587,
    589,
    590,
    591,
    591,
    592,
    593,
    594,
    595,
    595,
    596,
    596,
    597,
    597,
    598,
    598,
    599,
    599,
    600,
    600,
    601,
    601,
    601,
    602,
    602,
    602,
    603,
    603,
    603,
    604,
    604,
    604,
  ];
  @override
  void initState() {
    // TODO: implement initState
    //QuranAPI.searchForAyah('ا');
    surahsName =ConstantData.surahsName;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return
       Stack(
        children: [
          Container(
            height: height,
            width: width,
            color: Colors.white,
          ),
          Container(
            height: height * 0.55,
            width: width,
            decoration: BoxDecoration(
              color: MainTheme.primaryColor,
              borderRadius:
                  const BorderRadius.only(bottomRight: Radius.circular(95)),
            ),
            child: Center(
              child: SvgPicture.asset(MainTheme.ArabicDesign2),
            ),
          ),
          ListView.separated(
            shrinkWrap: true,
            itemCount:114,
            padding: EdgeInsets.only(top: 8),
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                child: Center(
                  child: Container(
                    height: 70,
                    width: 0.88 * width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [

                              Text(index==113 ? '1': (surahsPageNumber[index+1]-surahsPageNumber[index]).toString(),style:TextStyle(fontSize: 20 ,color: MainTheme.primaryColor,fontWeight: FontWeight.w800),),
                              Text(
                                'صفحة',
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w800,
                                    color: MainTheme.primaryColor),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(surahsName[index],textAlign: TextAlign.right,
                              style: TextStyle(
                                  fontSize: 19,
                                  fontWeight: FontWeight.w800,
                                  color: MainTheme.primaryColor),),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                onTap: (){
                   Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => QuranView(intialPage:621-(surahsPageNumber[index]-1),versionNumber: ConstantData.currentQuranVersion)),
                  );
                },
              );
            }, separatorBuilder: (BuildContext context, int index) {
              return SizedBox(height: 10,);
          },
          )
        ],
      );

  }
}
