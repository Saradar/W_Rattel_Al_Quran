import 'package:flutter/material.dart';
import 'package:hivetemplate/constants/constant_data.dart';

import '../constants/MainTheme.dart';
class SettingsView extends StatefulWidget {
  const SettingsView({Key? key}) : super(key: key);

  @override
  _SettingsViewState createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  String quranVersion = '';
  @override
  void initState() {
    quranVersion = ConstantData.getTheCurrentQuranVersion();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MainTheme.primaryColor,
        centerTitle: true,
        automaticallyImplyLeading: true,
        title: Text(
          'الاعدادات',
          textAlign: TextAlign.right,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w800,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(child: Text('النسخة الحالية للقران:')),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(child: Text(quranVersion)),
            ),
            SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(child: Text('ل تغيير النسخة')),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  child: Text('أخضر',style: TextStyle(color: Colors.white),),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green
                  ),
                  onPressed: (){
                    setState(() {
                      quranVersion = 'أخضر';
                      ConstantData.currentQuranVersion = 1;
                      Navigator.of(context)
                          .pushNamedAndRemoveUntil('/start', (Route<dynamic> route) => false);
                    });
                  },
                ),
                ElevatedButton(
                  child: Text('أزرق',style: TextStyle(color: Colors.white),),
                  style: ElevatedButton.styleFrom(
                      primary: Colors.blue
                  ),
                  onPressed: (){
                    setState(() {
                      quranVersion = 'أزرق';
                      ConstantData.currentQuranVersion = 2;
                      Navigator.of(context)
                          .pushNamedAndRemoveUntil('/start', (Route<dynamic> route) => false);
                    });
                  },
                ),
                ElevatedButton(
                  child: Text('أسود' ,style: TextStyle(color: Colors.white),),
                  style: ElevatedButton.styleFrom(
                      primary: Colors.black
                  ),
                  onPressed: (){
                    setState(() {
                      quranVersion = 'أسود';
                      ConstantData.currentQuranVersion = 3;
                      Navigator.of(context)
                          .pushNamedAndRemoveUntil('/start', (Route<dynamic> route) => false);
                    });
                  },
                ),
              ],
            )

          ],
        ),
      ),
    );
  }
}
