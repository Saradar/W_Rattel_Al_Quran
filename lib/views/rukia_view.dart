import 'dart:async';
import 'dart:io';

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/link.dart';
import 'package:audioplayers/audioplayers.dart';

import '../constants/MainTheme.dart';
import '../constants/constant_data.dart';
import '../widgets/custom_card.dart';
import 'audio/playing_controls.dart';
import 'audio/postion_seek_widget.dart';

class RukiaView extends StatefulWidget {
  const RukiaView({Key? key}) : super(key: key);

  @override
  State<RukiaView> createState() => _RukiaViewState();
}

class _RukiaViewState extends State<RukiaView> {
  late AssetsAudioPlayer _assetsAudioPlayer;
  final List<StreamSubscription> _subscriptions = [];
  var audios = <Audio>[];
  @override
  void initState() {
    // TODO: implement initState
    audios = [
      Audio.file('${ConstantData.dir.path}/rukia/rukia.mp3',
          metas: Metas(
            id: '0',
            title: 'الرقية الشرعية',
            artist: '',)
      ),
      Audio.file('${ConstantData.dir.path}/rukia/rukia.mp3',
          metas: Metas(
            id: '1',
            title: 'الرقية الشرعية',
            artist: '',)
      ),
    ];
    _assetsAudioPlayer = AssetsAudioPlayer.newPlayer();
    _subscriptions.add(_assetsAudioPlayer.playlistAudioFinished.listen((data) {
      print('playlistAudioFinished : $data');
    }));
    _subscriptions.add(_assetsAudioPlayer.audioSessionId.listen((sessionId) {
      print('audioSessionId : $sessionId');
    }));
    openPlayer();
    super.initState();
  }

  void openPlayer() async {
    await _assetsAudioPlayer.open(
      Playlist(audios: audios, startIndex: 0),
      showNotification: true,
      autoStart: false,
      notificationSettings: NotificationSettings(
          seekBarEnabled: true,
          stopEnabled: true,
          customStopAction: (player){
            _assetsAudioPlayer.pause();
            _assetsAudioPlayer.seek(Duration(seconds: 0));
          }
        //prevEnabled: false,
        //customNextAction: (player) {
        //  print('next');
        //}
        //customStopIcon: AndroidResDrawable(name: 'ic_stop_custom'),
        //customPauseIcon: AndroidResDrawable(name:'ic_pause_custom'),
        //customPlayIcon: AndroidResDrawable(name:'ic_play_custom'),
      ),
    );
  }

  Audio find(List<Audio> source, String fromPath) {
    return source.firstWhere((element) => element.path == fromPath);
  }



  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: MainTheme.primaryColor,
          title: Text(
            'الرقية الشرعية',
            textAlign: TextAlign.right,
            style: TextStyle(
                fontWeight: FontWeight.w800, fontSize: 18, color: Colors.white),
          ),
          automaticallyImplyLeading: true,
        ),
        body: Stack(
          children: [
            Container(
              height: height,
              width: width,
              color: Colors.white,
            ),
            Column(
              children: [
                Container(
                  height: height * 0.40,
                  width: width,
                  decoration: BoxDecoration(
                    color: MainTheme.primaryColor,
                    //borderRadius: const BorderRadius.only(bottomRight: Radius.circular(70)),
                  ),
                  child: SvgPicture.asset(MainTheme.ArabicDesign),
                ),
                Expanded(
                  child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 48.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          SizedBox(
                            height: 20,
                          ),
                          StreamBuilder<Playing?>(
                              stream: _assetsAudioPlayer.current,
                              builder: (context, playing) {
                                if (playing.data != null) {
                                  final myAudio = find(audios,
                                      playing.data!.audio.assetAudioPath);

                                  return Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Text(
                                              myAudio.metas.title.toString(),
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.w800,
                                                  color:
                                                  MainTheme.primaryColor),
                                              textAlign: TextAlign.center),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Text(
                                            myAudio.metas.artist.toString(),
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.w800,
                                                color: MainTheme.primaryColor),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }
                                return SizedBox.shrink();
                              }),
                          _assetsAudioPlayer.builderCurrent(
                              builder: (context, Playing? playing) {
                                return Column(
                                  children: <Widget>[
                                    _assetsAudioPlayer.builderLoopMode(
                                      builder: (context, loopMode) {
                                        return PlayerBuilder.isPlaying(
                                            player: _assetsAudioPlayer,
                                            builder: (context, isPlaying) {
                                              return PlayingControls(
                                                loopMode: loopMode,
                                                isPlaying: isPlaying,
                                                isPlaylist: true,
                                                onStop: () {
                                                  _assetsAudioPlayer.pause();
                                                  _assetsAudioPlayer.seek(Duration(seconds: 0));
                                                },
                                                toggleLoop: () {
                                                  _assetsAudioPlayer.toggleLoop();
                                                },
                                                onPlay: () {
                                                  _assetsAudioPlayer.playOrPause();
                                                },
                                                onNext: () {
                                                  //_assetsAudioPlayer.forward(Duration(seconds: 10));
                                                  _assetsAudioPlayer.next();
                                                },
                                                onPrevious: () {
                                                  _assetsAudioPlayer.previous();
                                                },
                                              );
                                            });
                                      },
                                    ),
                                    _assetsAudioPlayer.builderRealtimePlayingInfos(
                                        builder:
                                            (context, RealtimePlayingInfos? infos) {
                                          if (infos == null) {
                                            return SizedBox();
                                          }
                                          //print('infos: $infos');
                                          return Column(
                                            children: [
                                              PositionSeekWidget(
                                                currentPosition: infos.currentPosition,
                                                duration: infos.duration,
                                                seekTo: (to) {
                                                  _assetsAudioPlayer.seek(to);
                                                },
                                              ),
                                            ],
                                          );
                                        }),
                                  ],
                                );
                              }),
                          SizedBox(
                            height: 20,
                          ),
                          /* _assetsAudioPlayer.builderCurrent(
                          builder: (BuildContext context, Playing? playing) {
                            return SongsSelector(
                              audios: audios,
                              onPlaylistSelected: (myAudios) {
                                _assetsAudioPlayer.open(
                                  Playlist(audios: myAudios),
                                  showNotification: true,
                                  headPhoneStrategy:
                                  HeadPhoneStrategy.pauseOnUnplugPlayOnPlug,
                                  audioFocusStrategy: AudioFocusStrategy.request(
                                      resumeAfterInterruption: true),
                                );
                              },
                              onSelected: (myAudio) async {
                                try {
                                  await _assetsAudioPlayer.open(
                                    myAudio,
                                    autoStart: false,
                                    showNotification: true,
                                    playInBackground: PlayInBackground.enabled,
                                    audioFocusStrategy: AudioFocusStrategy.request(
                                        resumeAfterInterruption: true,
                                        resumeOthersPlayersAfterDone: true),
                                    headPhoneStrategy: HeadPhoneStrategy.pauseOnUnplug,
                                    notificationSettings: NotificationSettings(
                                      //seekBarEnabled: false,
                                      //stopEnabled: true,
                                      //customStopAction: (player){
                                      //  player.stop();
                                      //}
                                      //prevEnabled: false,
                                      //customNextAction: (player) {
                                      //  print('next');
                                      //}
                                      //customStopIcon: AndroidResDrawable(name: 'ic_stop_custom'),
                                      //customPauseIcon: AndroidResDrawable(name:'ic_pause_custom'),
                                      //customPlayIcon: AndroidResDrawable(name:'ic_play_custom'),
                                    ),
                                  );
                                } catch (e) {
                                  print(e);
                                }
                              },
                              playing: playing,
                            );
                          }),*/
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ));
  }
}
