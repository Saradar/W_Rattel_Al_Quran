import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hivetemplate/constants/constant_data.dart';
import 'package:hivetemplate/views/quran_view.dart';

import '../constants/MainTheme.dart';
class JuzListView extends StatefulWidget {
  const JuzListView({Key? key}) : super(key: key);

  @override
  _JuzListViewState createState() => _JuzListViewState();
}

class _JuzListViewState extends State<JuzListView> {
  List<String>juzNames = [
    'الجزء الأول',
    'الجزء الثاني',
    'الجزء الثالث',
    'الجزء الرابع',
    'الجزء الخامس',
    'الجزء السادس',
    'الجزء السابع',
    'الجزء الثامن',
    'الجزء التاسع',
    'الجزء العاشر',
    'الجزء الحادي عشر',
    'الجزء الثاني عشر',
    'الجزء الثالث عشر',
    'الجزء الرابع عشر',
    'الجزء الخامس عشر',
    'الجزء السادس عشر',
    'الجزء السابع عشر',
    'الجزء الثامن عشر',
    'الجزء التاسع عشر',
    'الجزء العشرون',
    'الجزء الحادي والعشرون',
    'الجزء الثاني والعشرون',
    'الجزء الثالث والعشرون',
    'الجزء الرابع والعشرون',
    'الجزء الخامس والعشرون',
    'الجزء السادس والعشرون',
    'الجزء السابع والعشرون',
    'الجزء الثامن والعشرون',
    'الجزء التاسع والعشرون',
    'الجزء الثلاثون',
  ];

  List<int>juzPagesNum = [621,600,580,560,540,520,500,480,460,440,420,400,380,360,340,320,300,280,260,240,
  220,200,180,160,140,120,100,80,60,40];
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return

       Stack(
        children: [
          Container(
            height: height,
            width: width,
            color: Colors.white,
          ),
          Container(
            height: height * 0.55,
            width: width,
            decoration: BoxDecoration(
              color: MainTheme.primaryColor,
              borderRadius:
              const BorderRadius.only(bottomRight: Radius.circular(95)),
            ),
            child: Center(
              child: SvgPicture.asset(MainTheme.ArabicDesign2),
            ),
          ),
          ListView.separated(
            shrinkWrap: true,
            itemCount:30,
            padding: EdgeInsets.only(top: 8),
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                child: Center(
                  child: Container(
                    height: 70,
                    width: 0.88 * width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Center(
                      child: Text(juzNames[index],style:TextStyle(fontSize: 20 ,color: MainTheme.primaryColor,fontWeight: FontWeight.w800),textAlign: TextAlign.right,),
                    ),
                  ),
                ),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => QuranView(intialPage:juzPagesNum[index],versionNumber: ConstantData.currentQuranVersion,)),
                  );
                },
              );
            }, separatorBuilder: (BuildContext context, int index) {
            return SizedBox(height: 10,);
          },
          )
        ],
      );

  }
}
