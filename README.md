# Wa_Rattel_Al_Quran

An Islamic app with a simple and beautiful UI, The app has many Sections , to read the Holy_Quran with diffrent Colors and Designs ,and Search for a Specific Ayah /Surah/Juz , Get the prayers Times for your current location , Read the Azkar , and Listen To audio Qruran and Ruqia .

I used for this app multiple technology :
- NoSql Local Database (Hive Package) to Store Some Information for the User 
- Firestore Storage to Store the Audio Files , to unable the user to download them or playing them as stream 
- Third party Api to unable user get  the Arabic Date and Prayers Time 
- I made an Architecture of models to store the Quran Locally in the user device , to unable user Search for Surah / Ayah
- I tried to Implement MVC Architecture

And Here is some Screens from the App:
